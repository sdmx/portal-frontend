schema {
  query: RootQuery
  mutation: Mutation
}

"""
Input, Scalar, & Enum
"""
scalar DateTime
scalar File

enum Direction {
  ASC
  DESC
}
enum Privilege {
  PUBLIC
  INTERNAL
}
enum Status {
  ACTIVE
  INACTIVE
}
enum ApprovalStatus {
  DRAFT
  APPROVED
  REJECTED
  PROCESSED
}

input Order {
  by: String!
  direction: Direction
}

"""
Queries
"""
interface Node {
  id: ID!
}

type RootQuery {
  viewer: Viewer!
  node(id: ID!): Node
}

type Viewer implements Node {
  id: ID!

  # Category
  allCategories(
    first: Int
    offset: Int
    filter: String
    order: Order
    after: ID
  ): Connection
  category(id: ID!): Category

  # News
  allNews(
    first: Int
    offset: Int
    filter: String
    order: Order
    after: ID
  ): Connection
  news(id: ID!): News
}

"""
Mutations
"""
type Mutation {
  # Category
  createCategory(input: CategoryInput!): CategoryPayload
  updateCategory(input: CategoryInput!, id: ID!): CategoryPayload
  deleteCategory(id: ID!): CategoryPayload

  # News
  createNews(input: NewsInput!): NewsPayload
  updateNews(input: NewsInput!, id: ID!): NewsPayload
  deleteNews(id: ID!): NewsPayload
}

input CategoryInput {
  name: String!
  parentId: ID
}
type CategoryPayload {
  node: Category
}

input NewsInput {
  name: String!
  body: String!
  links: [String]
  thumbnail: File
  categories: [ID]
}
type NewsPayload {
  node: News
}

"""
Connection
"""
type PageInfo {
  hasNextPage: Boolean!
  hasPreviousPage: Boolean!
  startCursor: ID!
  endCursor: ID!
}
type Edge {
  cursor: String!
  node: Node!
}
type Connection {
  totalCount: Int
  pageInfo: PageInfo!
  edges: [Edge!]! @listLength(min: 3, max: 10)
}

"""
Types
"""
type User implements Node {
  id: ID!
  username: String! @fake(type: domainName)
  name: String! @fake(type: firstName)
  email: String @fake(type: email)
  status: Status!
  organisation: Organisation
  position: Position
}
type Organisation implements Node {
  id: ID!
  name: String! @fake(type: companyBS)
  parent: Organisation
}
type Position implements Node {
  id: ID!
  name: String! @fake(type: productName)
  parent: Position
  hasOrganisation: Boolean
}

# Category
type Category implements Node {
  id: ID!
  name: String! @fake(type: financeAccountName)
  parent: Category
  children: [Category]
}

# News
type News implements Node {
  id: ID!
  name: String! @fake(type: words)
  body: String @fake(type: words)
  links: [String] @listLength(min: 0, max: 2) @fake(type: url)
  thumbnail: File @fake(type: imageUrl)
  status: Status!
  privilege: Privilege!
  isPublished: Boolean!
  created: DateTime @fake(type: pastDate)
  updated: DateTime @fake(type: pastDate)
  user: User!
  categories: [Category]
}
type NewsApproval implements Node {
  id: ID!
  news: News!
  user: User!
  status: ApprovalStatus!
  created: DateTime! @fake(type: pastDate)
  updated: DateTime! @fake(type: pastDate)
}

# Publication
type Publication implements Node {
  id: ID!
  name: String! @fake(type: words)
  body: String @fake(type: words)
  links: [String] @listLength(min: 0, max: 2) @fake(type: url)
  thumbnail: String @fake(type: imageUrl)
  status: Status!
  privilege: Privilege!
  isPublished: Boolean!
  created: DateTime! @fake(type: pastDate)
  updated: DateTime! @fake(type: pastDate)
  user: User!
  categories: [Category]
  attachments: [PublicationAttachment]
}
type PublicationAttachment implements Node {
  id: ID!
  displayName: String! @fake(type: filename)
  filepath: String! @fake(type: filename)
  created: DateTime @fake(type: pastDate)
}
type PublicationApproval implements Node {
  id: ID!
  publication: Publication!
  user: User!
  status: ApprovalStatus!
  created: DateTime! @fake(type: pastDate)
  updated: DateTime! @fake(type: pastDate)
}
