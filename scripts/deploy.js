const fs = require('fs-extra');
const args = process.argv.slice(2);
const buildDir = "./build";
const targetDir = args[0] + "/src/main/resources/public/";

fs.removeSync(targetDir);
fs.copy(buildDir, targetDir)
  .then(() => console.log(`Successfully copied to ${targetDir}`))
  .catch(err => console.error(err));
