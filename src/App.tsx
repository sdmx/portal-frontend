import React, { Suspense } from "react";
import { RawIntlProvider, FormattedMessage } from "react-intl";
import { Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { Layout } from "antd";
import { Viewer } from "types/schema";

import { setLocale } from "actions/app";
import Header from "components/Layout/Header";
import Footer from "components/Layout/Footer";
import Error from "components/Error";
import Loading from "components/Loading";
import routes from "./routes";

import "./App.css";

interface Props {
  app: any;
  retry: (() => void) | null;
  setAppLocale: (locale: string) => void;
  viewer: Viewer;
}

class App extends React.Component<Props> {
  componentDidMount() {
    const { app, setAppLocale } = this.props;
    setAppLocale(app.locale);
  }

  render() {
    const { app, viewer } = this.props;

    return (
      <Layout className="App">
        {app.intl && (
          <RawIntlProvider value={app.intl}>
            <Header user={viewer.user} />

            <Layout.Content className="ph3">
              <Suspense fallback={<Loading />}>
                <Switch>
                  {routes.map(
                    ({ path, exact, Component }: LayoutType.MenuType, i) =>
                      Component && (
                        <Route key={i} exact={exact} path={path}>
                          <Component />
                        </Route>
                      )
                  )}
                  <Route path={"*"}>
                    <Error message={<FormattedMessage id="error.404" />} />
                  </Route>
                </Switch>
              </Suspense>
            </Layout.Content>
            <Footer />
          </RawIntlProvider>
        )}
      </Layout>
    );
  }
}

const mapStateToProps = ({ app }: any) => ({ app });
const mapDispatchToProps = (dispatch: Dispatch) => ({
  setAppLocale: setLocale(dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
