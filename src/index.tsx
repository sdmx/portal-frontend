import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from "react-redux";
import { graphql, QueryRenderer, Variables } from "react-relay";
import { Viewer } from "types/schema";

import environment from "relayEnvironment";
import Error from "components/Error";
import Loading from "components/Loading";

import * as serviceWorker from "./serviceWorker";
import store from "./store";

import "./styles/tachyons.min.css";
import "./index.css";
import { refresh } from "utils/auth";

const App = React.lazy(() => import("./App"));
const AdminApp = React.lazy(() => import("./app/admin/App"));
const strictMode = false;

// eslint-disable-next-line
const consoleError = console.error.bind(console);

// eslint-disable-next-line
console.error = (err: any, ...args: any[]) => {
  if (
    err.message &&
    err.message.startsWith("[React Intl Error MISSING_TRANSLATION]")
  ) {
    return;
  }

  consoleError(err, ...args);
};

class Content extends React.Component {
  state = { refresh: false };

  componentDidMount() {
    refresh().then(() => this.setState({ refresh: true }));
  }

  render() {
    const { refresh } = this.state;

    if (!refresh) {
      return <Loading />;
    }

    return (
      <QueryRenderer<{ variables: Variables; response: { viewer: Viewer } }>
        environment={environment}
        variables={{}}
        render={({ error, props, retry }) => {
          if (error) {
            return <Error message={error.message} />;
          } else if (props) {
            return (
              <Router>
                <Switch>
                  <Route path="/admin">
                    <AdminApp viewer={props.viewer} retry={retry} />
                  </Route>
                  <Route path="/">
                    <App viewer={props.viewer} retry={retry} />
                  </Route>
                </Switch>
              </Router>
            );
          }

          return <Loading />;
        }}
        query={graphql`
          query srcQuery {
            viewer {
              id
              user {
                name
              }
            }
          }
        `}
      />
    );
  }
}

const MainApp = (
  <Suspense fallback={<Loading />}>
    <Provider store={store}>
      <Content />
    </Provider>
  </Suspense>
);

ReactDOM.render(
  strictMode ? <React.StrictMode> {MainApp} </React.StrictMode> : MainApp,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
