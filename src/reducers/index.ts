import { combineReducers } from "redux";

import admin from "app/admin/reducers";
import app from "./app";

export default combineReducers({
  app,
  admin,
});
