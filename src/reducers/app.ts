import { RECEIVE_LOCALE, REQUEST_LOCALE } from "actions/app";
import { defaultLocale } from "locale/index";
import { ReduxAction } from "types/redux";

const defaultState = {
  locale: defaultLocale,
  isLoading: false,
};

export default (state = defaultState, { type, payload }: ReduxAction) => {
  switch (type) {
    case REQUEST_LOCALE:
      return { ...state, isLoading: true };

    case RECEIVE_LOCALE:
      const { intl, locale } = payload;

      return {
        ...state,
        isLoading: false,
        intl,
        locale,
        fmt: intl.formatMessage,
      };

    default:
      return state;
  }
};
