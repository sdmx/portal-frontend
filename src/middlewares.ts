import { applyMiddleware, Dispatch, Middleware } from "redux";
import { createLogger } from "redux-logger";

const localStorage: Middleware<any, any, Dispatch> = ({ getState }) => {
  return (next: Dispatch) => (action: any) => {
    const returnValue = next(action);
    const { app, layout } = getState();

    // save state into local storage
    const state = { app: { locale: app.locale }, layout };
    window.localStorage.setItem("app", JSON.stringify(state));

    return returnValue;
  };
};

export const createMiddleware = () => {
  if (process.env.NODE_ENV === "production") {
    return applyMiddleware(localStorage);
  } else {
    return applyMiddleware(localStorage, createLogger());
  }
};
