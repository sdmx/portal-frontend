import React from "react";
import { Tooltip, Button } from "antd";
import { EditFilled } from "@ant-design/icons";
import { FormattedMessage } from 'react-intl';
import { Link } from "react-router-dom";

interface Props {
  url: string;
}

const EditButton = ({ url }: Props) => (
  <Tooltip title={<FormattedMessage id="action.edit" />}>
    <Button>
      <Link to={url}>
        <EditFilled />
      </Link>
    </Button>
  </Tooltip>
);

export default EditButton;
