import React from "react";
import { PlusOutlined } from "@ant-design/icons";
import { Button } from "antd";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";

interface Props {
  url: string;
}

const CreateButton = ({ url }: Props) => (
  <Button type="primary">
    <Link to={url}>
      <PlusOutlined /> <FormattedMessage id="action.new" />
    </Link>
  </Button>
);

export default CreateButton;
