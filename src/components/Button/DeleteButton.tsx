import React from "react";
import { Button, Popconfirm, Tooltip } from "antd";
import { DeleteFilled } from "@ant-design/icons";
import { FormattedMessage } from "react-intl";

interface Props {
  onConfirm: () => void;
  confirmMessage?: any;
}

const DeleteButton = ({ onConfirm, confirmMessage }: Props) => {
  const message = confirmMessage || (
    <FormattedMessage id={"info.confirmation"} />
  );

  return (
    <Tooltip title={<FormattedMessage id="action.delete" />}>
      <Popconfirm title={message} onConfirm={onConfirm}>
        <Button danger>
          <DeleteFilled />
        </Button>
      </Popconfirm>
    </Tooltip>
  );
};

export default DeleteButton;
