import React from "react";
import { Upload } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { UploadFile } from "antd/lib/upload/interface";

interface Props {
  onRemove: (file: UploadFile) => void;
  files?: UploadFile[];
}

interface States {
  files: UploadFile[];
}

class InputFileDragger extends React.Component<Props> {
  state: States = { files: [] };

  componentDidMount() {
    this.setState({ files: this.props.files });
  }

  validate(file: UploadFile) {
    this.setState(({ files }: States) => ({ files: [...files, file] }));
    return false; // prevent auto upload
  }

  render() {
    const { onRemove, ...attrs } = this.props;
    const { files } = this.state;

    return (
      <Upload.Dragger
        multiple={true}
        beforeUpload={this.validate}
        fileList={files}
        onRemove={onRemove ? onRemove : undefined}
        {...attrs}
      >
        <p className="ant-upload-drag-icon">
          <UploadOutlined />
        </p>
        <p className="ant-upload-text">Click or drag file to upload</p>
      </Upload.Dragger>
    );
  }
}

export default InputFileDragger;
