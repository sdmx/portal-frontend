import React from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import "./TextEditor.css";

interface Props {
  onChange?: (value: string) => void;
  value?: any;
}

class TextEditor extends React.Component<Props> {
  render() {
    const { value, onChange } = this.props;

    return (
      <ReactQuill
        theme="snow"
        value={value || ""}
        onChange={onChange}
        modules={{
          toolbar: [
            [{ header: "1" }, { header: "2" }, { font: [] }],
            [{ size: [] }],
            ["bold", "italic", "underline", "strike", "blockquote"],
            [
              { list: "ordered" },
              { list: "bullet" },
              { indent: "-1" },
              { indent: "+1" },
            ],
            ["link", "image", "video"],
            ["clean"],
          ],
          clipboard: {
            // toggle to add extra line breaks when pasting HTML:
            matchVisual: false,
          },
        }}
      />
    );
  }
}

export default TextEditor;
