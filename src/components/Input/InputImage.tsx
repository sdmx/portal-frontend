import React from "react";
import { Upload } from "antd";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import { UploadFile } from "antd/lib/upload/interface";

interface Props {
  onChange?: (file: UploadFile) => void;
  value?: string;
  imgAlt?: string;
}

interface States {
  loading: boolean;
  value: string;
}

// FIXME: Warning: [antd: Upload] `value` is not validate prop, do you mean `fileList`?
class InputImage extends React.Component<Props> {
  state: States = { loading: false, value: "" };

  componentDidMount() {
    this.setState({ value: this.props.value });
  }

  getBase64(img: any, callback: (imageUrl: any) => void) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  validate() {
    return false; // prevent auto upload
  }

  setPreview(file: UploadFile) {
    this.setState({ loading: true });

    if (file) {
      this.getBase64(file, (imageUrl) =>
        this.setState({
          value: imageUrl,
          loading: false,
        })
      );
    } else {
      this.setState({
        value: "error",
        loading: false,
      });
    }
  }

  render() {
    const { onChange, imgAlt, ...attrs } = this.props;
    const { value, loading } = this.state;

    return (
      <Upload
        listType="picture-card"
        beforeUpload={this.validate}
        showUploadList={false}
        onChange={(info) => {
          this.setPreview(info.file);

          if (!!onChange) {
            onChange(info.file);
          }
        }}
        {...attrs}
      >
        {!loading && value ? (
          <img
            src={value}
            alt={imgAlt ? imgAlt : ""}
            style={{ maxWidth: "100%", maxHeight: "100%" }}
          />
        ) : (
          <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div className="ant-upload-text">Upload</div>
          </div>
        )}
      </Upload>
    );
  }
}

export default InputImage;
