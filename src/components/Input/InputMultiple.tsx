import React from "react";
import { injectIntl, IntlShape } from "react-intl";
import { Input, Popconfirm, Button, Row, Col } from "antd";
import { PlusOutlined, DeleteOutlined } from "@ant-design/icons";
import style from "./InputMultiple.module.css";

interface Props {
  onChange?: (value: any) => any;
  value?: any;
  placeholder?: string;
  intl: IntlShape;
}

interface ValueItem {
  id: number;
  value?: string;
  showConfirm?: boolean;
}

let counter = 0;

class InputMultiple extends React.Component<Props> {
  state: { values: ValueItem[] } = { values: [] };

  componentDidMount() {
    const { value = [] } = this.props;

    if (value.length > 0) {
      value.forEach((item: string) => this.add(item));
    } else {
      this.add();
    }
  }

  add(value?: string) {
    const { values } = this.state;

    counter++;
    values.push({ id: counter, value });

    this.setState({ values });
    this.triggerChange(values);
  }

  remove(index: number) {
    const values = this.state.values.filter(({ id }) => id !== index);
    this.setState({ values });
    this.triggerChange(values);
  }

  triggerChange(updatedValue: ValueItem[]) {
    const { onChange } = this.props;

    if (onChange) {
      onChange(updatedValue.map(({ value }) => value));
    }
  }

  handleInputChange(value: string, id: number) {
    const values = this.state.values.map((item) => {
      if (item.id === id) {
        item.value = value;
      }
      return item;
    });

    this.setState({ values });
    this.triggerChange(values);
  }

  handleConfirmVisibleChange(visible: boolean, condition: boolean, id: number) {
    const values = this.state.values.map((item) => {
      if (item.id === id) {
        item.showConfirm = visible;
      }
      return item;
    });

    if (!visible) {
      this.setState({ values });
      return;
    }

    if (condition) {
      this.remove(id);
    } else {
      this.setState({ values }); // show the popconfirm
    }
  }

  render() {
    const { intl, placeholder } = this.props;
    const { values } = this.state;

    return (
      <div>
        <Button type="primary" size="small" onClick={() => this.add()}>
          <PlusOutlined />
        </Button>

        <div className={style.wrapper}>
          {values.map(({ id, value, showConfirm = false }: ValueItem) => (
            <Row gutter={[8, 8]} className={style.container} key={id}>
              <Col className="gutter-row" span={20}>
                <Input
                  value={value}
                  placeholder={placeholder}
                  onChange={(e) => this.handleInputChange(e.target.value, id)}
                />
              </Col>

              <Col className="gutter-row" span={4}>
                {/* Remove Button */}
                {values.length > 1 && (
                  <Popconfirm
                    placement="left"
                    title={intl.formatMessage({ id: "info.confirmation" })}
                    onConfirm={() => this.remove(id)}
                    visible={showConfirm}
                    onVisibleChange={(visible) =>
                      this.handleConfirmVisibleChange(visible, !value, id)
                    }
                  >
                    <Button
                      className={showConfirm ? "" : style.child}
                      type="danger"
                    >
                      <DeleteOutlined />
                    </Button>
                  </Popconfirm>
                )}
              </Col>
            </Row>
          ))}
        </div>
      </div>
    );
  }
}

export default injectIntl(InputMultiple);
