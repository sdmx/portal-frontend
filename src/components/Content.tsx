import React, { ReactNode } from "react";

import "./Content.css";

interface Props {
  children: ReactNode;
}

export default ({ children }: Props) => (
  <div className="content">{children}</div>
);
