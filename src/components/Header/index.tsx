import React from "react";
import { Menu, Row, Col } from "antd";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";

import UserLogin from "./UserLogin";
import HeaderBar from "./HeaderBar";

import style from "./index.module.css";

const menu: any[] = [
  { url: "/data", label: <FormattedMessage id="app.dataService" /> },
  { url: "/publication", label: <FormattedMessage id="app.publication" /> },
  { url: "/news", label: <FormattedMessage id="app.news" /> },
];

interface Props {
  user: any;
}

class Header extends React.Component<Props> {
  render() {
    const { user } = this.props;

    return (
      <div className={style.header}>
        <HeaderBar />

        <Row className={style.headerContainer}>
          <Col span={4} className={style.logo}>
            <Link to="/">
              <img src="/img/logo.png" alt="Logo" />
            </Link>
          </Col>

          <Col span={20}>
            <Row>
              <Col span={18}>
                <Menu mode="horizontal">
                  {menu.map(({ url, label }) => (
                    <Menu.Item key={url}>
                      <Link to={url}>
                        <b>{label}</b>
                      </Link>
                    </Menu.Item>
                  ))}
                </Menu>
              </Col>
              <Col span={6} className={style.userLogin}>
                <UserLogin user={user} />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Header;
