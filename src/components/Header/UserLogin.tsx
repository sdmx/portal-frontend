import React from "react";
import { LogoutOutlined, DownSquareOutlined } from "@ant-design/icons";
import { Dropdown, Menu } from "antd";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import { injectIntl, IntlShape } from "react-intl";

import style from "./UserLogin.module.css";
import { logout, redirectLogout } from "utils/auth";

interface Props extends RouteComponentProps {
  user?: any;
  intl: IntlShape;
}

class UserLogin extends React.Component<Props> {
  logout() {
    logout();
    window.location.href = redirectLogout;
  }

  render() {
    const { user, intl } = this.props;

    return (
      <div className={style.userLogin}>
        {user ? (
          <Dropdown
            trigger={["click"]}
            placement="bottomRight"
            overlay={
              <Menu onClick={({ key }) => (key === "logout" && this.logout())}>
                <Menu.Item key="logout">
                  <LogoutOutlined />
                  {intl.formatMessage({
                    id: "action.logout",
                    defaultMessage: "Logout",
                  })}
                </Menu.Item>
              </Menu>
            }
          >
            <div>
              <span>{user.name}</span>
              <DownSquareOutlined />
            </div>
          </Dropdown>
        ) : (
          <Link to="/login">
            <b>
              {intl.formatMessage({
                id: "action.login",
                defaultMessage: "Login",
              })}
            </b>
          </Link>
        )}
      </div>
    );
  }
}

export default withRouter(injectIntl(UserLogin));
