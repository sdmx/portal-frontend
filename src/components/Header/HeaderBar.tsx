import React from "react";
import moment from "moment";
import { Col, Row, Button } from "antd";
import { connect } from "react-redux";

import { setLocale } from "actions/app";

import style from "./HeaderBar.module.css";

const HeaderBar = ({ setAppLocale }: { setAppLocale: any }) => (
  <Row className={style.headerBar}>
    <Col span={12} className={style.menu}>
      <div className={style.lang}>
        <Button
          type="link"
          className={style.langItem}
          onClick={() => setAppLocale("en")}
        >
          English
        </Button>
        <span className={style.divider}>|</span>
        <Button
          type="link"
          className={style.langItem}
          onClick={() => setAppLocale("id")}
        >
          Indonesia
        </Button>
      </div>
    </Col>
    <Col span={12} className={style.right}>
      {moment().format("DD MMMM YYYY")}
    </Col>
  </Row>
);

export default connect(
  (props) => props,
  (dispatch) => ({
    setAppLocale: setLocale(dispatch),
  })
)(HeaderBar);
