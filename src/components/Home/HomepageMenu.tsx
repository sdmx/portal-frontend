import React from "react";
import { Link } from "react-router-dom";
import { Col, Row } from "antd";
import {
  LineChartOutlined,
  PicRightOutlined,
  FileTextOutlined,
  FileSearchOutlined,
} from "@ant-design/icons";

import style from "./HomepageMenu.module.css";
import { FormattedMessage } from "react-intl";

const HomepageMenu = () => (
  <Row className={style.homepageMenu}>
    <Col span={6} className={style.menuItem}>
      <Link to="/data/internal">
        <LineChartOutlined />
        <div className={style.title}>
          <FormattedMessage id="statistics" />
        </div>
      </Link>
    </Col>
    <Col span={6} className={style.menuItem}>
      <Link to="/news">
        <PicRightOutlined />
        <div className={style.title}>
          <FormattedMessage id="app.news" />
        </div>
      </Link>
    </Col>
    <Col span={6} className={style.menuItem}>
      <Link to="/publication">
        <FileTextOutlined />
        <div className={style.title}>
          <FormattedMessage id="app.publication" />
        </div>
      </Link>
    </Col>
    <Col span={6} className={style.menuItem}>
      <Link to="/">
        <FileSearchOutlined />
        <div className={style.title}>
          <FormattedMessage id="open data" />
        </div>
      </Link>
    </Col>
  </Row>
);

export default HomepageMenu;
