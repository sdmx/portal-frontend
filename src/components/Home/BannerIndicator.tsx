import React from "react";
import moment from "moment";
import classNames from "classnames";

import style from "./BannerIndicator.module.css";

const indicator = [
  {
    label: "GDP Growth Rate",
    value: -0.4,
    diff: 1.7,
    period: moment().format("YYYY/MM"),
  },
  {
    label: "CPI",
    value: 104.88,
    diff: 0.7,
    period: moment().format("YYYY/MM"),
  },
  {
    label: "PPI",
    value: 103.73,
    diff: 0.4,
    period: moment().format("YYYY/MM"),
  },
  {
    label: "Current Account",
    value: 4949,
    diff: 0,
    period: moment().format("YYYY/MM"),
  },
  {
    label: "M2 (Average)",
    value: 2763,
    diff: 6.6,
    period: moment().format("YYYY/MM"),
  },
];

class BannerIndicator extends React.Component {
  render() {
    return (
      <div className={style.bannerIndicator}>
        <div className={style.title}>Principal Indicators</div>
        <table className={style.table} cellPadding="7px">
          {indicator.map((item) => {
            let diffValue;

            if (item.diff === 0) {
              diffValue = "-";
            } else if (item.diff > 0) {
              diffValue = `+${item.diff}`;
            } else {
              diffValue = item.diff;
            }

            return (
              <tr>
                <td>{item.label}</td>
                <th>{item.value}</th>
                <th className={classNames({ [style.danger]: item.diff > 0 })}>
                  {diffValue}
                </th>
                <td>{item.period}</td>
              </tr>
            );
          })}
        </table>
      </div>
    );
  }
}

export default BannerIndicator;
