import React from "react";
import moment from "moment";
import { FormattedMessage } from "react-intl";

import style from "./DailyIndicator.module.css";

const dailyIndicator = [
  { label: "CD (91 DAYS)", status: "Closed", time: moment().format("HH:mm") },
  { label: "TB (3 YEARS)", status: "Closed", time: moment().format("HH:mm") },
  {
    label: "CB (3 YEARS, AA-)",
    status: "Closed",
    time: moment().format("HH:mm"),
  },
  { label: "USD/KRW", status: "Closed", time: moment().format("HH:mm") },
  { label: "USD/JPY", status: "Closed", time: moment().format("HH:mm") },
];

const DailyIndicator = () => (
  <div className={style.dailyIndicator}>
    <div className={style.title}>
      <FormattedMessage id="daily indicator" />
    </div>
    <table className={style.table} cellPadding="5px">
      {dailyIndicator.map((item) => (
        <tr>
          <td>{item.label}</td>
          <td>{item.time}</td>
          <td>{item.status}</td>
        </tr>
      ))}
    </table>
  </div>
);

export default DailyIndicator;
