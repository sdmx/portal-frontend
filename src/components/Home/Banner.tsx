import React from "react";
import Slider from "react-slick";
import { Row, Col } from "antd";
import Plot from "react-plotly.js";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import dataInflasi from "data/samples/indicator/inflasi.json";
import dataBi7days from "data/samples/indicator/bi-7days.json";
import dataKursBiUsd from "data/samples/indicator/kursBI-USD.json";
import dataKursJisdorUsd from "data/samples/indicator/kursJISDOR-USD.json";
import BannerIndicator from "./BannerIndicator";
import style from "./Banner.module.css";

const plotLayout: Partial<Plotly.Layout> = {
  height: 200,
  showlegend: false,
  paper_bgcolor: "rgba(0,0,0,0)",
  plot_bgcolor: "rgba(0,0,0,0)",
  font: { color: "#fff" },
  margin: { l: 40, r: 40, t: 20, b: 40 },
};

const plotConfig: Partial<Plotly.Config> = {
  displayModeBar: false,
  responsive: true,
};

class Banner extends React.Component {
  render() {
    return (
      <Row className={style.banner}>
        {/* Logo */}
        <Col span={6} className={style.logo}>
          <img src="/img/logo-inverse.png" alt="Logo Bank Indonesia" />
        </Col>

        {/* Charts */}
        <Col span={12}>
          <Slider
            swipe={true}
            autoplaySpeed={3000}
            autoplay={true}
            adaptiveHeight={true}
            vertical={false}
            dots={false}
            arrows={false}
            infinite={true}
            slidesToShow={1}
            slidesToScroll={1}
            speed={500}
          >
            {/* Inflasi */}
            <div className={style.sliderItem}>
              <div className={style.title}>Tingkat Inflasi</div>
              <Plot
                layout={plotLayout}
                config={plotConfig}
                data={[
                  {
                    type: "scatter",
                    mode: "lines+markers",
                    name: "Tingkat Inflasi",
                    marker: { color: "#fff" },
                    y: dataInflasi.map(({ value }) => value),
                    x: dataInflasi.map(({ period }) => period),
                  },
                ]}
              />
            </div>

            {/* BI 7 Days */}
            <div className={style.sliderItem}>
              <div className={style.title}>7 Days</div>
              <Plot
                layout={plotLayout}
                config={plotConfig}
                data={[
                  {
                    type: "scatter",
                    mode: "lines+markers",
                    name: "Repo Rate",
                    marker: { color: "#fff" },
                    y: dataBi7days.map(({ value }) => value),
                    x: dataBi7days.map(({ date }) => date),
                  },
                ]}
              />
            </div>

            {/* Kurs JISDOR-USD */}
            <div className={style.sliderItem}>
              <div className={style.title}>Kurs Referensi JISDOR USD-IDR</div>
              <Plot
                layout={plotLayout}
                config={plotConfig}
                data={[
                  {
                    type: "scatter",
                    mode: "lines+markers",
                    name: "Kurs",
                    marker: { color: "#fff" },
                    y: dataKursJisdorUsd.map(({ value }) => value),
                    x: dataKursJisdorUsd.map(({ date }) => date),
                  },
                ]}
              />
            </div>

            {/* Kurs BI-USD */}
            <div className={style.sliderItem}>
              <div className={style.title}>Kurs USD</div>
              <Plot
                layout={plotLayout}
                config={plotConfig}
                data={[
                  {
                    type: "scatter",
                    name: "Kurs Jual",
                    mode: "lines+markers",
                    marker: { color: "#fff" },
                    y: dataKursBiUsd.map(({ jual }) => jual),
                    x: dataKursBiUsd.map(({ date }) => date),
                  },
                  {
                    type: "scatter",
                    name: "Kurs Beli",
                    mode: "lines+markers",
                    marker: { color: "#f90" },
                    y: dataKursBiUsd.map(({ beli }) => beli),
                    x: dataKursBiUsd.map(({ date }) => date),
                  },
                ]}
              />
            </div>
          </Slider>
        </Col>

        {/* Indicators */}
        <Col span={6} className={style.indicator}>
          <BannerIndicator />
        </Col>
      </Row>
    );
  }
}

export default Banner;
