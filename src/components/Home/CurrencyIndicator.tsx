import React from "react";
import { Space } from 'antd';
import { CaretLeftOutlined, CaretRightOutlined } from "@ant-design/icons";

import style from "./CurrencyIndicator.module.css";

const statsIndicator = [
  { name: "USD", values: [2.9233, 2.9331] },
  { name: "EUR", values: [3.1965, 3.1967] },
  { name: "GBP", values: [4.3489, 4.3571] },
  { name: "EURO/USD", values: [1.0979, 1.098] },
  { name: "BIST 30", values: [3.346, 3.345] },
];

const CurrencyIndicator = () => {
  return (
    <div className={style.currencyIndicator}>
      <div>
        {statsIndicator.map(({ name, values }) => (
          <Space key={name} className={style.item}>
            <b>{name}</b>
            <span>
              <CaretLeftOutlined />
              {values[0]}
            </span>
            <span>
              <CaretRightOutlined />
              {values[1]}
            </span>
          </Space>
        ))}
      </div>
    </div>
  );
};

export default CurrencyIndicator;
