import React from "react";
import { Link } from "react-router-dom";

import style from "./DatasetSubjects.module.css";

const dsSubjects = [
  "finance",
  "population",
  "education",
  "environment",
  "government",
  "science",
  "region",
  "legal",
  "traffic",
  "agriculture",
  "energy",
  "health",
];

class DatasetSubjects extends React.Component {
  render() {
    return (
      <div className={style.datasetSubjects}>
        <div className={style.title}>DATASET</div>
        <div>
          {dsSubjects.map((subject) => (
            <div key={subject} className={style.item}>
              <Link to={`/`}>
                <div>
                  <img
                    src={`/img/icon/ds-subjects/${subject}.png`}
                    alt={subject}
                  />
                </div>
                <div className={style.itemName}>{subject}</div>
              </Link>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default DatasetSubjects;
