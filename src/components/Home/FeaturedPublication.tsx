import React from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import { FormattedMessage } from "react-intl";

import style from "./FeaturedPublication.module.css";

class FeaturedPublication extends React.Component {
  render() {
    const data: any[] = [];

    return (
      <div className={style.featuredPublication}>
        <div className={style.title}>
          <FormattedMessage id="app.publication" />
        </div>
        <table className={style.table} cellPadding="5px">
          {data.slice(0, 3).map(({ id, name, timestamp }: any) => (
            <tr>
              <td>
                <Link to={`/content/publication/${id}`}>{name}</Link>
              </td>
              <td className={style.timestamp}>
                {timestamp || moment().format("DD MMM YYYY")}
              </td>
            </tr>
          ))}
        </table>
      </div>
    );
  }
}

export default FeaturedPublication;
