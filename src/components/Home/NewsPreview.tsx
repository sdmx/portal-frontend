import React from "react";
import { Link } from "react-router-dom";
import { Col, Row } from "antd";
import { fileUrl, truncate } from "utils/string";

import style from "./NewsPreview.module.css";
import { FormattedMessage } from 'react-intl';

class NewsPreview extends React.Component {
  render() {
    const data: any[] = [];

    return (
      <div className={style.newsPreview}>
        <div className={style.title}>
          <FormattedMessage id="app.news" />
        </div>
        <div>
          {data.slice(-1).map(({ id, name, thumbnail }) => (
            <Link to={`/content/news/${id}`} className={style.item}>
              <Row key={id}>
                <Col span={6} className={style.thumbnail}>
                  <img alt={name} src={fileUrl(thumbnail)} />
                </Col>
                <Col span={14} className={style.title}>
                  {truncate(name, 40)}
                </Col>
              </Row>
            </Link>
          ))}
        </div>
      </div>
    );
  }
}

export default NewsPreview;
