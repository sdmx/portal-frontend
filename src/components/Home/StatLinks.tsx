import React from "react";
import { Link } from "react-router-dom";
import { PieChartFilled, LineChartOutlined } from "@ant-design/icons";

import style from "./StatLinks.module.css";

class StatLinks extends React.Component {
  render() {
    return (
      <div className={style.statLinks}>
        <Link to="/" className={style.visualStat}>
          <span>VISUAL STAT</span>
          <PieChartFilled />
        </Link>

        <Link to="/" className={style.indonesiaStat}>
          <span>INDONESIA STAT</span>
          <LineChartOutlined />
        </Link>
      </div>
    );
  }
}

export default StatLinks;
