import React from "react";
import moment from "moment";
import classnames from "classnames";

import style from "./Commodity.module.css";
import { FormattedMessage } from "react-intl";

const commodity = [
  {
    label: "DUBAI",
    value: "00:00",
    status: "=",
    dateTime: moment().format("DD MMM"),
  },
  {
    label: "BRENT",
    value: "00:01",
    status: "+",
    dateTime: moment().format("DD MMM"),
  },
  {
    label: "GOLD",
    value: "00:01",
    status: "+",
    dateTime: moment().format("DD MMM"),
  },
  {
    label: "COPPER",
    value: "1:50",
    status: "-",
    dateTime: moment().format("DD MMM"),
  },
];

const Commodity = () => {
  return (
    <div className={style.commodity}>
      <div className={style.title}>
        <FormattedMessage id="commodity" />
      </div>
      <table className={style.table} cellPadding="5px">
        {commodity.map((item) => (
          <tr>
            <td>{item.label}</td>
            <td
              className={classnames({
                [style.tr]: true,
                [style.danger]: item.status === "+",
              })}
            >
              {item.status !== "=" && item.status}
              {item.value}
            </td>
            <td className={style.tr}>{item.dateTime}</td>
          </tr>
        ))}
      </table>
    </div>
  );
};

export default Commodity;
