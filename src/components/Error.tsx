import React from "react";
import { Space } from "antd";
import { ExclamationCircleOutlined } from "@ant-design/icons";

import "./Error.css";

interface Props {
  message: any;
}

const Error = ({ message }: Props) => (
  <div className="error">
    <Space>
      <ExclamationCircleOutlined />
      {message}
    </Space>
  </div>
);

export default Error;
