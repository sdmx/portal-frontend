import React from "react";
import { FormattedMessage } from "react-intl";
import { Space } from 'antd';
import { ExclamationCircleOutlined } from "@ant-design/icons";

import "./Empty.css";

interface Props {
  message?: string;
}

const Empty = ({ message }: Props) => (
  <div className="empty">
    <Space>
      <ExclamationCircleOutlined />
      {message || <FormattedMessage id={"info.empty"} />}
    </Space>
  </div>
);

export default Empty;
