import React from "react";
import moment from "moment";
import { Link } from "react-router-dom";
import Img from "react-image-fallback";
import { Pagination, Input } from "antd";

import Empty from "components/Empty";
import { injectIntl, IntlShape } from "react-intl";

interface Content {
  id: string;
  name: string;
  thumbnail: string;
  created: string;
}

interface Props {
  title: string;
  data?: any[] | null;
  intl: IntlShape;
  onPaginate?: (page: number, pageSize?: number) => void;
  onSearch?: (query: string, pageSize?: number) => void;
  urlResolver?: (data: Content) => string;
  pageSize?: number;
}

class ListItem extends React.Component<Props> {
  render() {
    const {
      data,
      intl,
      onPaginate,
      onSearch,
      pageSize = 10,
      title,
      urlResolver,
    } = this.props;

    const totalPages = 1;
    const pageNum = 1;
    const count = 1;

    return (
      <div>
        <div className="bg-primary-dark tc pv3 white f3">
          {title.toUpperCase()}
        </div>
        <div className="pa2 bl br bb black-20">
          <Input.Search
            allowClear
            className="navbar-search"
            onSearch={(query) => onSearch && onSearch(query, pageSize)}
            placeholder={intl.formatMessage({ id: "action.search" })}
          />
        </div>
        <div className="pa2 bl br bb black-20">
          {(data && data.length > 0) || <Empty />}
          {data &&
            data.map((item: Content) => (
              <Link
                key={item.id}
                to={urlResolver ? urlResolver(item) : "#"}
                className="black-80 dib mv1 w-100"
              >
                <div className="flex">
                  <div
                    className="w-20 br2 overflow-hidden"
                    style={{ height: "40px" }}
                  >
                    <Img
                      src={item.thumbnail}
                      fallbackImage="/img/default/content.png"
                      alt={item.name}
                    />
                  </div>
                  <div className="w-80 ph2">
                    <div className="b truncate">{item.name}</div>
                    <div className="gray f7">
                      {moment(item.created).format("DD MMM YYYY | HH:mm")}
                    </div>
                  </div>
                </div>
              </Link>
            ))}

          <div className="tc">
            {totalPages > 1 && (
              <Pagination
                total={count}
                current={pageNum}
                pageSize={pageSize}
                onChange={onPaginate}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(ListItem);
