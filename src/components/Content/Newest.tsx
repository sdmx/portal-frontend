import React from 'react';
import { Carousel } from "antd";
import moment from 'moment';
import Img from 'react-image-fallback';
import { Link } from 'react-router-dom';

import { striptags } from 'utils/string';
import ContentSlider from './ContentSlider';

interface Content {
  id: string;
  name: string;
  thumbnail: string;
  created: string;
}

interface Props {
  data?: any[] | null;
  urlResolver?: (data: Content) => string;
}

class Newest extends React.Component<Props> {
  render() {
    const { data, urlResolver } = this.props;

    return (
      <div className="w-100 h-100">
        <Carousel autoplay className="ph2">
          {data &&
            data.map((item) => (
              <ContentSlider
                key={item.id}
                title={item.name}
                href={urlResolver ? urlResolver(item) : '#'}
                content={item.body}
                created={item.created}
                thumbnail={item.thumbnail}
              />
            ))}
        </Carousel>

        <div className="flex justify-between">
          {data && data.splice(1, 3).map((item) => (
            <div key={item.id} className="w-33 ph2 b--black-10">
              <Link
                title={item.name}
                className="black-70 hover-black"
                to={urlResolver ? urlResolver(item) : '#'}
              >
                <div className="overflow-hidden" style={{ height: "100px" }}>
                  <Img
                    src={item.thumbnail}
                    fallbackImage="/img/default/content.png"
                    className="w-100"
                    alt={item.name}
                  />
                </div>
                <div className="b pv1 truncate"> {item.name} </div>
                <div className="f7 gray">
                  {moment(item.created).format("DD MMM YYYY | HH:mm")}
                </div>
                <div className="f7">
                  {striptags(item.body).length > 60
                    ? `${striptags(item.body).substr(0, 60)}...`
                    : striptags(item.body)}
                </div>
              </Link>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default Newest;
