import React from "react";
import Img from "react-image-fallback";
import { Link } from "react-router-dom";
import Shiitake from "shiitake";

interface Content {
  id: string;
  name: string;
  thumbnail: string;
  created: string;
}

interface Props {
  data?: any[] | null;
  urlResolver?: (data: Content) => string;
}

class ListItem extends React.Component<Props> {
  render() {
    const { data = [], urlResolver } = this.props;

    return (
      <div className="bg-primary-dark">
        {data && data.length > 0 && (
          <div>
            <div className="b white pv2">
              <div className="tc pv1">Highlight this week</div>
              <Link
                key={data[0].id}
                className="dib w-100 bb b--white-10 white-70 hover-white f7"
                to={urlResolver ? urlResolver(data[0]) : "#"}
              >
                <div className="overflow-hidden" style={{ maxHeight: "200px" }}>
                  <Img
                    src={data[0].thumbnail}
                    fallbackImage="/img/default/content.png"
                    className="w-100"
                    alt={data[0].name}
                  />
                </div>
                <Shiitake lines={1} className="pa2">
                  {data[0].name}
                </Shiitake>
              </Link>
            </div>
            <div className="ph2">
              {data.splice(1).map((item: Content) => (
                <Link
                  key={item.id}
                  className="db b w-100 pv2 bb b--white-10 white-80 hover-white f7"
                  to={urlResolver ? urlResolver(item) : "#"}
                >
                  <Shiitake lines={1}>{item.name}</Shiitake>
                </Link>
              ))}
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default ListItem;
