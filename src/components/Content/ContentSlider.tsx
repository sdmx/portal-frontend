import React from "react";
import Img from "react-image-fallback";
import moment from "moment";
import { Link } from "react-router-dom";

import { striptags } from "utils/string";

interface Props {
  title: string;
  thumbnail: string;
  href: string;
  content: string;
  created: string;
}

const ContentSlider = ({ title, thumbnail, href, content, created }: Props) => {
  const _content = striptags(content);

  return (
    <div className="pb3">
      <div
        className="relative w-100 overflow-hidden"
        style={{ height: "400px" }}
      >
        <Img
          src={thumbnail}
          fallbackImage="/img/default/publication.png"
          alt={title}
          style={{ width: "100%" }}
          className="center absolute"
        />
        <div
          className="absolute w-100"
          style={{
            bottom: 0,
            height: "80%",
            backgroundImage:
              "linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,0.8))",
          }}
        >
          <Link to={href} className="white-80 hover-white">
            <div className="absolute w-100 pa1" style={{ bottom: 0 }}>
              <div className="ph2">
                <div className="b f3">{title}</div>
                <div className="pv2">
                  {moment(created).format("DD MMM YYYY | HH:mm")}
                </div>
              </div>
              <div className="bt bb b--white-20 mb2">
                <div className="f7 w-100 pa2 bt bb b--white-20">
                  {_content.length > 100
                    ? `${_content.substr(0, 100)}...`
                    : _content}
                </div>
              </div>
            </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default ContentSlider;
