import React from "react";

interface Props {
  label?: string;
  children: any;
}

export default ({ label, children }: Props) => (
  <div className="mb3">
    {label && <p className="b">{label}</p>}
    {children}
  </div>
);
