import React from "react";
import { Col, Row } from "antd";

import app from "data/app.json";
import FooterBar from "./FooterBar";
import SocialMedia from "./SocialMedia";
import style from "./index.module.css";

class Footer extends React.Component {
  render() {
    return (
      <div className={style.footer}>
        <Row className={style.footerContainer}>
          <Col span={18}>
            <div className={style.title}>{app.company.name}</div>
            <p>{app.company.address}</p>
            <p>{app.company.contact}</p>
            <p>{app.company.email}</p>
          </Col>
          <Col span={6}>
            <SocialMedia />
          </Col>
        </Row>

        <FooterBar />
      </div>
    );
  }
}

export default Footer;
