import React from "react";
import Img from "react-image-fallback";
import { Link } from "react-router-dom";
import { striptags, fileUrl } from "utils/string";

const DESC_LIMIT = 200;

const ListItem = ({ name, url = "#", icon, body, thumbnail }: any) => (
  <div className="flex mv1 pt1 pb2 ph3 bb b--moon-gray">
    <div className="w-20 pr3">
      <Img
        alt={name}
        src={fileUrl(thumbnail)}
        fallbackImage="/img/default/publication.png"
      />
    </div>
    <div className="w-80">
      <div className="flex">
        <Link to={url} className="text-primary f6 b">
          {name}
        </Link>
        {icon && icon}
      </div>
      {body && (
        <p className="ma0 f7">
          {body.length > DESC_LIMIT
            ? `${striptags(body).substring(0, DESC_LIMIT)}...`
            : striptags(body)}
        </p>
      )}
    </div>
  </div>
);

export default ListItem;
