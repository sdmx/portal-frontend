import React from "react";
import ListItem from "components/Search/ListItem";
import { ProfileOutlined } from "@ant-design/icons";

class NewsResults extends React.Component<any> {
  render() {
    const { data = [] } = this.props;

    return (
      <div>
        {data.length > 0 ? (
          <div className="w-50-l">
            {data.map(({ id, ...attrs }: any) => (
              <ListItem
                key={id}
                url={`/content/news/${id}`}
                icon={<ProfileOutlined />}
                {...attrs}
              />
            ))}
          </div>
        ) : (
          <div className="f3 silver">No Search Results Found.</div>
        )}
      </div>
    );
  }
}

export default NewsResults;
