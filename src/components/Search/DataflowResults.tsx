import React from "react";
import { Link } from "react-router-dom";

class DataflowResults extends React.Component {
  render() {
    const urlResolver = (url: string) => url;
    const data: any[] = [];

    return (
      <div>
        {data.length > 0 ? (
          <div className="w-60-l">
            {data.map(({ id, name }) => (
              <div key={id} className="mv1 pv1 flex bb b--moon-gray">
                <Link to={urlResolver(id)} className="text-primary f6">
                  {name}
                </Link>
              </div>
            ))}
          </div>
        ) : (
          <div className="f3 silver">No Search Results Found.</div>
        )}
      </div>
    );
  }
}

export default DataflowResults;
