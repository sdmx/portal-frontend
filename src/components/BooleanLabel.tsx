import React from "react";
import { Tag } from "antd";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";

class BooleanLabel extends React.Component<{ value: any }> {
  render() {
    if (this.props.value) {
      return (
        <Tag color="success">
          <CheckOutlined />
        </Tag>
      );
    }

    return (
      <Tag color="error">
        <CloseOutlined />
      </Tag>
    );
  }
}

export default BooleanLabel;
