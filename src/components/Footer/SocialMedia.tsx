import React from "react";
import {
  FacebookOutlined,
  TwitterOutlined,
  YoutubeOutlined,
  InstagramOutlined,
} from "@ant-design/icons";

import app from "data/app.json";
import style from "./SocialMedia.module.css";

class SocialMedia extends React.Component {
  render() {
    return (
      <div className={style.socialMedia}>
        <div className={style.title}>Follow Us</div>
        <div>
          <a
            href={app.company.social.facebook}
            rel="noopener noreferrer"
            target="_blank"
          >
            <FacebookOutlined />
          </a>
          <a
            href={app.company.social.twitter}
            rel="noopener noreferrer"
            target="_blank"
          >
            <TwitterOutlined />
          </a>
          <a
            href={app.company.social.youtube}
            rel="noopener noreferrer"
            target="_blank"
          >
            <YoutubeOutlined />
          </a>
          <a
            href={app.company.social.instagram}
            rel="noopener noreferrer"
            target="_blank"
          >
            <InstagramOutlined />
          </a>
        </div>

        <div>
          <a
            href={app.company.social.playstore}
            rel="noopener noreferrer"
            target="_blank"
          >
            <img
              className="mv1 pr1"
              src="/img/icon/playstore.png"
              width="100px"
              alt="Play Store"
            />
          </a>
          <a
            href={app.company.social.appstore}
            rel="noopener noreferrer"
            target="_blank"
          >
            <img
              className="mv1 pr1"
              src="/img/icon/appstore.png"
              width="100px"
              alt="App Store"
            />
          </a>
        </div>
      </div>
    );
  }
}

export default SocialMedia;
