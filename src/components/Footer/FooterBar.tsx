import React from "react";
import moment from "moment";
import { Link } from "react-router-dom";
import { Col, Row } from "antd";

import style from "./FooterBar.module.css";

class Footer extends React.Component {
  render() {
    return (
      <Row className={style.footerBar}>
        <Col span={12}>
          copyright &copy; {moment().format("YYYY")} Bank Indonesia All Rights
          Reserved
        </Col>
        <Col span={12} className={style.right}>
          <Link to="/">Legal Notice</Link>
          <Link to="/">Security</Link>
          <Link to="/">Privacy Policy</Link>
          <Link to="/">Information</Link>
          <Link to="/">Services</Link>
        </Col>
      </Row>
    );
  }
}

export default Footer;
