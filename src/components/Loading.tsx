import React from "react";
import { LoadingOutlined } from "@ant-design/icons";

import "./Loading.css";

const Loading = () => (
  <div className="loading">
    <LoadingOutlined />
  </div>
);

export default Loading;
