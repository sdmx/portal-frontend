import React from "react";
import {
  FilePdfFilled,
  FileTextFilled,
  HomeFilled,
  UnorderedListOutlined,
  UserOutlined,
} from "@ant-design/icons";

const routes: LayoutType.MenuType[] = [
  {
    path: "/admin",
    label: "app.home",
    Icon: HomeFilled,
    exact: true,
    Component: React.lazy(() => import("./views/Home")),
  },
  {
    path: "/admin/user",
    label: "app.user",
    Icon: UserOutlined,
    Component: React.lazy(() => import("./views/User")),
  },
  {
    path: "/admin/category",
    label: "app.category",
    Icon: UnorderedListOutlined,
    Component: React.lazy(() => import("./views/Category")),
  },
  {
    path: "/admin/news",
    label: "app.news",
    Icon: FileTextFilled,
    Component: React.lazy(() => import("./views/News")),
  },
  {
    path: "/admin/publication",
    label: "app.publication",
    Icon: FilePdfFilled,
    Component: React.lazy(() => import("./views/Home")),
  },
];

export default routes;
