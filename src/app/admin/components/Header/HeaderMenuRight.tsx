import React from "react";
import { Menu, Dropdown } from "antd";
import { Link } from "react-router-dom";

import SubMenu from "./HeaderSubMenu";
import style from "./HeaderMenu.module.css";

interface Props {
  menu: LayoutType.MenuType[];
}

class HeaderMenu extends React.Component<Props> {
  static defaultProps = {
    menu: []
  };

  render() {
    const { menu } = this.props;

    return (
      <Menu mode="horizontal" className={style.HeaderMenu}>
        {menu &&
          menu.map(({ subs, label, path, onClick, Icon }: LayoutType.MenuType) => {
            if (!subs) {
              return (
                <Menu.Item key={label}>
                  <Link to={path} onClick={onClick}>
                    {label}
                  </Link>
                </Menu.Item>
              );
            }

            return (
              <Menu.Item key={label}>
                <Dropdown
                  placement="bottomLeft"
                  overlay={<SubMenu menu={subs} />}
                >
                  <div className={style.dropdown}>
                    {Icon && <Icon />}
                    {label}
                  </div>
                </Dropdown>
              </Menu.Item>
            );
          })}
      </Menu>
    );
  }
}

export default HeaderMenu;
