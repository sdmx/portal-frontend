import React from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";

import style from "./HeaderMenu.module.css";

interface Props {
  menu: LayoutType.MenuType[];
}

class HeaderMenu extends React.Component<Props> {
  render() {
    const { menu } = this.props;

    return (
      <Menu mode="horizontal" className={style.HeaderMenu}>
        {menu &&
          menu.map(({ path, label, html }: LayoutType.MenuType) => (
            <Menu.Item key={path}>
              {html ? <a href={path}>{label}</a> : <Link to={path}>{label}</Link>}
            </Menu.Item>
          ))}
      </Menu>
    );
  }
}

export default HeaderMenu;
