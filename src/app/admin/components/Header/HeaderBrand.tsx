import React from "react";
import { Link } from "react-router-dom";

import logoIcon from "images/logo-inverse-icon.png";
import logo from "images/logo-inverse.png";
import style from "./HeaderBrand.module.css";

class HeaderBrand extends React.Component<{ collapse: boolean }> {
  static defaultProps = {
    collapse: false,
  }

  render() {
    const { collapse } = this.props;

    return collapse ? (
      <Link to="/admin" className={`${style.HeaderBrand} ${style.collapse}`}>
        <img src={logoIcon} alt="Logo" />
      </Link>
    ) : (
      <Link to="/admin" className={`${style.HeaderBrand}`}>
        <img src={logo} alt="Logo" />
      </Link>
    );
  }
}

export default HeaderBrand;
