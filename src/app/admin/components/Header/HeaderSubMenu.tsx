import React from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";

import style from "./HeaderSubMenu.module.css";

interface Props {
  menu: LayoutType.MenuType[];
}

class HeaderSubMenu extends React.Component<Props> {
  static defaultProps = {
    menu: []
  };

  render() {
    const { menu } = this.props;

    return (
      <Menu className={style.HeaderSubMenu}>
        {menu &&
          menu.map(({ Icon, path, onClick, label, html }: LayoutType.MenuType) => {
            const iconEle = Icon && <Icon />;

            return (
              <Menu.Item key={path}>
                {html ? (
                  <a href={path} onClick={onClick}>
                    {iconEle} {label}
                  </a>
                ) : (
                  <Link to={path} onClick={onClick}>
                    {iconEle} {label}
                  </Link>
                )}
              </Menu.Item>
            );
          })}
      </Menu>
    );
  }
}

export default HeaderSubMenu;
