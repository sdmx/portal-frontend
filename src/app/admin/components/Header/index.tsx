import React from "react";
import { connect } from "react-redux";
import { Layout } from "antd";
import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";

import { sidebarLeftCollapse } from "app/admin/actions/layout";
import HeaderBrand from "./HeaderBrand";
import HeaderMenu from "./HeaderMenu";
import HeaderMenuRight from "./HeaderMenuRight";
import style from "./index.module.css";
import { Dispatch } from "redux";

interface Props {
  layout: LayoutType.AdminLayoutType;
  menu: LayoutType.MenuType[];
  rightMenu: LayoutType.MenuType[];
  sidebarLeftCollapse: Function;
}

class Header extends React.Component<Props> {
  static defaultProps = {
    menu: [],
    rightMenu: []
  };

  render() {
    const { menu, rightMenu, layout } = this.props;

    return (
      <Layout.Header className={style.Header}>
        <div className={style.brand}>
          <HeaderBrand collapse={layout.sidebarLeftCollapse} />

          <div
            className={style.toggle}
            onClick={() =>
              this.props.sidebarLeftCollapse(!layout.sidebarLeftCollapse)
            }
          >
            {layout.sidebarLeftCollapse ? (
              <MenuUnfoldOutlined />
            ) : (
              <MenuFoldOutlined />
            )}
          </div>
        </div>

        <HeaderMenu menu={menu} />
        <HeaderMenuRight menu={rightMenu} />
      </Layout.Header>
    );
  }
}

const mapStateToProps = ({ admin }: any) => ({ layout: admin.layout });
const mapDispatchToProps = (dispatch: Dispatch) => ({
  sidebarLeftCollapse: (collapse: boolean) => dispatch(sidebarLeftCollapse(collapse))
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
