import React from "react";
import { Menu, Tooltip } from "antd";
import { Link, withRouter, RouteComponentProps } from "react-router-dom";

import style from "./SidebarMenu.module.css";

interface Props extends RouteComponentProps {
  menu: LayoutType.MenuType[];
  collapse: boolean;
}

class SidebarMenu extends React.Component<Props> {
  static defaultProps = {
    menu: [],
  };

  getActivePath = (pathname: string) => {
    const { menu } = this.props;

    for (const i in menu) {
      if (pathname.indexOf(menu[i].path) !== -1) {
        return [menu[i].path];
      }
    }

    return [];
  };

  render() {
    const { menu, collapse, location } = this.props;
    let activeMenu = "";

    menu.forEach(({ path }) => {
      if (
        location.pathname.indexOf(path) !== -1 &&
        path.length > activeMenu.length
      ) {
        activeMenu = path;
      }
    });

    return (
      <Menu
        mode="inline"
        defaultSelectedKeys={["0"]}
        selectedKeys={activeMenu.length > 0 ? [activeMenu] : []}
        className={`${style.SidebarMenu} ${style.collapse}`}
      >
        {menu &&
          menu.map(({ path, label, Icon }: LayoutType.MenuType, i) => (
            <Menu.Item key={path} className={style.menuItem}>
              <Tooltip
                placement="right"
                title={collapse ? label : null}
              >
                <Link to={path}>
                  {Icon && <Icon />}
                  <span>{!collapse && label}</span>
                </Link>
              </Tooltip>
            </Menu.Item>
          ))}
      </Menu>
    );
  }
}

export default withRouter(SidebarMenu);
