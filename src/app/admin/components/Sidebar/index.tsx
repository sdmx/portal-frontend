import React from "react";
import { Layout } from "antd";

import SidebarMenu from "./SidebarMenu";
import style from "./index.module.css";

interface Props {
  menu: LayoutType.MenuType[];
  collapse: boolean;
}

class Sidebar extends React.Component<Props> {
  static defaultProps = {
    menu: [],
    collapse: false,
  };

  render() {
    const { menu, collapse } = this.props;

    return (
      <Layout.Sider
        collapsible
        trigger={null}
        width={collapse ? 71 : 230}
        className={style.Sidebar}
      >
        <Layout.Content>
          <SidebarMenu menu={menu} collapse={collapse} />
        </Layout.Content>
      </Layout.Sider>
    );
  }
}

export default Sidebar;
