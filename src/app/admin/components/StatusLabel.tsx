import React from "react";
import { Tag } from "antd";

enum StatusEnum {
  "ACTIVE",
  "INACTIVE",
}

interface Props {
  value?: StatusEnum;
}

const colors: { [key in StatusEnum]: string } = {
  [StatusEnum.INACTIVE]: "default",
  [StatusEnum.ACTIVE]: "success",
};

class StatusLabel extends React.Component<Props> {
  render() {
    const { value } = this.props;

    return value && <Tag color={colors[value]}>{value}</Tag>;
  }
}

export default StatusLabel;
