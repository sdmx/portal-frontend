import React from "react";
import { Table } from "antd";
import { PaginationProps } from "antd/lib/pagination";
import { ColumnsType } from "antd/lib/table";
import { Connection, Maybe } from 'types/schema';

interface Props {
  isLoading?: boolean;
  totalCount: number;
  pageSize: number;
  data?: Maybe<Connection>;
  columns: ColumnsType<any>;
  onFetch: (variables: any) => void;
}

class TableData extends React.Component<Props> {
  state = { currentPage: 1 };

  componentWillReceiveProps(nextProps: Props) {
    if (
      !!nextProps.totalCount &&
      this.props.totalCount !== nextProps.totalCount
    ) {
      this.setState({ currentPage: 1 });
    }
  }

  fetch(pagination: PaginationProps = {}, sorter: any = {}) {
    const { onFetch, pageSize: defaultPageSize } = this.props;

    if (onFetch) {
      const { current = 1, pageSize = defaultPageSize } = pagination;
      const order = sorter.column
        ? {
            by: sorter.column.key,
            direction: sorter.order === "descend" ? "DESC" : "ASC",
          }
        : null;

      this.setState({ currentPage: current });

      onFetch({
        order,
        first: pageSize,
        offset: (current - 1) * pageSize,
      });
    }
  }

  render() {
    const {
      columns,
      pageSize,
      data,
      totalCount,
      isLoading = false,
    } = this.props;

    return (
      <Table
        bordered
        size="small"
        columns={columns}
        loading={isLoading}
        dataSource={data?.edges ? data?.edges : []}
        rowKey={({ node }) => node?.id}
        onChange={(pagination, _, sorter) => this.fetch(pagination, sorter)}
        pagination={{
          pageSize,
          current: this.state.currentPage,
          total: totalCount || 0,
          position: "bottomCenter",
        }}
      />
    );
  }
}

export default TableData;
