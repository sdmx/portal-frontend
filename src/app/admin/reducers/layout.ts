import { ADMIN_SIDEBAR_LEFT_COLLAPSE } from "../actions/layout";
import { ReduxAction } from 'types/redux';

const defaultState: LayoutType.AdminLayoutType = {
  sidebarLeftCollapse: false
};

export default (
  state: LayoutType.AdminLayoutType = defaultState,
  { type, payload }: ReduxAction
) => {
  switch (type) {
    case ADMIN_SIDEBAR_LEFT_COLLAPSE:
      return { ...state, sidebarLeftCollapse: payload.collapse };

    default:
      return state;
  }
};
