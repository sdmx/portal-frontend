export const ADMIN_SIDEBAR_LEFT_COLLAPSE = "ADMIN_SIDEBAR_LEFT_COLLAPSE";

export const sidebarLeftCollapse = (collapse: boolean) => ({
  type: ADMIN_SIDEBAR_LEFT_COLLAPSE,
  payload: { collapse },
});
