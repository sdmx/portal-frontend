import React, { Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import { RawIntlProvider, FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { Button, Layout } from "antd";

import routes from "./routes";
import { setLocale } from "actions/app";
import Error from "components/Error";
import Loading from "components/Loading";
import style from "./App.module.css";
import Header from "./components/Header";
import Sidebar from "./components/Sidebar";
import "./override.css";
import { Viewer } from 'types/schema';

interface Props {
  app: any;
  layout: LayoutType.AdminLayoutType;
  retry: (() => void) | null;
  setAppLocale: (locale: string) => void;
  viewer: Viewer;
}

class App extends React.Component<Props> {
  componentDidMount() {
    const { setAppLocale, app } = this.props;
    setAppLocale(app.locale);
  }

  render() {
    const { app, layout, setAppLocale } = this.props;

    return (
      <Layout className={style.App}>
        {app.intl && (
          <RawIntlProvider value={app.intl}>
            <Header />

            <Layout>
              <Sidebar collapse={layout.sidebarLeftCollapse} menu={routes} />

              <Layout>
                <Suspense fallback={<Loading />}>
                  <Switch>
                    {routes.map(
                      ({ path, exact, Component }: LayoutType.MenuType, i) =>
                        Component && (
                          <Route key={i} exact={exact} path={path}>
                            <Component />
                          </Route>
                        )
                    )}
                    <Route path={"/admin*"}>
                      <Error message={<FormattedMessage id="error.404" />} />
                    </Route>
                  </Switch>
                </Suspense>

                {/* Footer */}
                <div className={style.footer}>
                  <Button type="link" onClick={() => setAppLocale("en")}>
                    English
                  </Button>
                  <Button type="link" onClick={() => setAppLocale("id")}>
                    Indonesia
                  </Button>
                </div>
              </Layout>
            </Layout>
          </RawIntlProvider>
        )}
      </Layout>
    );
  }
}

const mapStateToProps = ({ app, admin }: any) => ({
  app,
  layout: admin.layout,
});
const mapDispatchToProps = (dispatch: Dispatch) => ({
  setAppLocale: setLocale(dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
