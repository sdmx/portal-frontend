import { commitMutation, graphql } from "react-relay";
import { Environment } from "relay-runtime";
import { NewsInput } from 'types/schema';
import { UploadableMap } from 'types/relay-runtime/definitions';

function commit(
  environment: Environment,
  input: NewsInput,
  uploadables: UploadableMap = {},
  id: string
) {
  return new Promise((resolve, reject) => {
    return commitMutation(environment, {
      mutation: graphql`
        mutation EditNewsMutation($id: ID!, $input: NewsInput!) {
          updateNews(id: $id, input: $input) {
            node {
              id
              name
              body
            }
          }
        }
      `,
      variables: { input, id },
      uploadables,
      onCompleted: resolve,
      onError: (err) => {
        console.log(err);
        reject(err);
      },
    });
  });
}

export default { commit };
