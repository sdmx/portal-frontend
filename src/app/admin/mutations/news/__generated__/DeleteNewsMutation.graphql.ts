/* tslint:disable */
/* eslint-disable */
/* @relayHash a4e9cec01c56aef2bd882782b5700e3c */

import { ConcreteRequest } from "relay-runtime";
export type DeleteNewsMutationVariables = {
    id: string;
};
export type DeleteNewsMutationResponse = {
    readonly deleteNews: {
        readonly node: {
            readonly id: string;
        } | null;
    } | null;
};
export type DeleteNewsMutation = {
    readonly response: DeleteNewsMutationResponse;
    readonly variables: DeleteNewsMutationVariables;
};



/*
mutation DeleteNewsMutation(
  $id: ID!
) {
  deleteNews(id: $id) {
    node {
      id
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "deleteNews",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "id",
        "variableName": "id"
      }
    ],
    "concreteType": "NewsPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "node",
        "storageKey": null,
        "args": null,
        "concreteType": "News",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "DeleteNewsMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "DeleteNewsMutation",
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "params": {
    "operationKind": "mutation",
    "name": "DeleteNewsMutation",
    "id": null,
    "text": "mutation DeleteNewsMutation(\n  $id: ID!\n) {\n  deleteNews(id: $id) {\n    node {\n      id\n    }\n  }\n}\n",
    "metadata": {}
  }
};
})();
(node as any).hash = 'da3047a5b062b134b8c43e000ae9f9d1';
export default node;
