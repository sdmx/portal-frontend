/* tslint:disable */
/* eslint-disable */
/* @relayHash b920d2aaed4cbddad884fc404dae38aa */

import { ConcreteRequest } from "relay-runtime";
export type NewsInput = {
    name: string;
    body: string;
    links?: Array<string | null> | null;
    thumbnail?: unknown | null;
    categories?: Array<string | null> | null;
};
export type EditNewsMutationVariables = {
    id: string;
    input: NewsInput;
};
export type EditNewsMutationResponse = {
    readonly updateNews: {
        readonly node: {
            readonly id: string;
            readonly name: string;
            readonly body: string | null;
        } | null;
    } | null;
};
export type EditNewsMutation = {
    readonly response: EditNewsMutationResponse;
    readonly variables: EditNewsMutationVariables;
};



/*
mutation EditNewsMutation(
  $id: ID!
  $input: NewsInput!
) {
  updateNews(id: $id, input: $input) {
    node {
      id
      name
      body
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "NewsInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "updateNews",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "id",
        "variableName": "id"
      },
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "NewsPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "node",
        "storageKey": null,
        "args": null,
        "concreteType": "News",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "name",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "body",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "EditNewsMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "EditNewsMutation",
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "params": {
    "operationKind": "mutation",
    "name": "EditNewsMutation",
    "id": null,
    "text": "mutation EditNewsMutation(\n  $id: ID!\n  $input: NewsInput!\n) {\n  updateNews(id: $id, input: $input) {\n    node {\n      id\n      name\n      body\n    }\n  }\n}\n",
    "metadata": {}
  }
};
})();
(node as any).hash = '4ed1cdc034aafd1b3a29e48c16e75568';
export default node;
