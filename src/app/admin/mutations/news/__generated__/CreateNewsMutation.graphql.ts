/* tslint:disable */
/* eslint-disable */
/* @relayHash b836e9d413cb355d84ed16d1d0450d64 */

import { ConcreteRequest } from "relay-runtime";
export type NewsInput = {
    name: string;
    body: string;
    links?: Array<string | null> | null;
    thumbnail?: unknown | null;
    categories?: Array<string | null> | null;
};
export type CreateNewsMutationVariables = {
    input: NewsInput;
};
export type CreateNewsMutationResponse = {
    readonly createNews: {
        readonly node: {
            readonly id: string;
            readonly name: string;
            readonly body: string | null;
        } | null;
    } | null;
};
export type CreateNewsMutation = {
    readonly response: CreateNewsMutationResponse;
    readonly variables: CreateNewsMutationVariables;
};



/*
mutation CreateNewsMutation(
  $input: NewsInput!
) {
  createNews(input: $input) {
    node {
      id
      name
      body
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "NewsInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createNews",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "NewsPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "node",
        "storageKey": null,
        "args": null,
        "concreteType": "News",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "name",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "body",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "CreateNewsMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateNewsMutation",
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "params": {
    "operationKind": "mutation",
    "name": "CreateNewsMutation",
    "id": null,
    "text": "mutation CreateNewsMutation(\n  $input: NewsInput!\n) {\n  createNews(input: $input) {\n    node {\n      id\n      name\n      body\n    }\n  }\n}\n",
    "metadata": {}
  }
};
})();
(node as any).hash = 'ace2c5175e9b16c5330778b7edce621e';
export default node;
