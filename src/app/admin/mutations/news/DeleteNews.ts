import { commitMutation, graphql } from "react-relay";
import { Environment } from "relay-runtime";

function commit(environment: Environment, id: string) {
  return new Promise((resolve, reject) =>
    commitMutation(environment, {
      mutation: graphql`
        mutation DeleteNewsMutation($id: ID!) {
          deleteNews(id: $id) {
            node {
              id
            }
          }
        }
      `,
      variables: { id },
      onCompleted: resolve,
      onError: (err) => {
        console.log(err);
        reject(err);
      },
    })
  );
}

export default { commit };
