import { commitMutation, graphql } from "react-relay";
import { Environment } from "relay-runtime";
import { NewsInput } from "types/schema";
import { UploadableMap } from "types/relay-runtime/definitions";

function commit(
  environment: Environment,
  input: NewsInput,
  uploadables: UploadableMap = {}
) {
  return new Promise((resolve, reject) => {
    return commitMutation(environment, {
      mutation: graphql`
        mutation CreateNewsMutation($input: NewsInput!) {
          createNews(input: $input) {
            node {
              id
              name
              body
            }
          }
        }
      `,
      variables: { input },
      uploadables,
      onCompleted: resolve,
      onError: (err) => {
        console.log(err);
        reject(err);
      },
    });
  });
}

export default { commit };
