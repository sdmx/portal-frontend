import { commitMutation, graphql } from "react-relay";
import { Environment } from "relay-runtime";
import { Viewer, CategoryInput } from "types/schema";

function commit(
  environment: Environment,
  viewer: Viewer,
  input: CategoryInput
) {
  return new Promise((resolve, reject) => {
    return commitMutation(environment, {
      mutation: graphql`
        mutation CreateCategoryMutation($input: CategoryInput!) {
          createCategory(input: $input) {
            node {
              id
              name
            }
          }
        }
      `,
      variables: { input },
      onCompleted: resolve,
      onError: (err) => {
        console.log(err);
        reject(err);
      },
      configs: [
        {
          type: "RANGE_ADD",
          parentID: viewer.id,
          edgeName: "createCategory",
          connectionInfo: [
            {
              key: "CategoryList_data",
              rangeBehavior: "append",
            },
          ],
        },
      ],
    });
  });
}

export default { commit };
