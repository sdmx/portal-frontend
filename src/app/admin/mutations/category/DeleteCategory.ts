import { commitMutation, graphql } from "react-relay";
import { Environment } from "relay-runtime";
import { Viewer } from 'types/schema';

function commit(environment: Environment, viewer: Viewer, id: string) {
  return new Promise((resolve, reject) =>
    commitMutation(environment, {
      mutation: graphql`
        mutation DeleteCategoryMutation($id: ID!) {
          deleteCategory(id: $id) {
            node {
              id
            }
          }
        }
      `,
      variables: { id },
      onCompleted: resolve,
      onError: (err) => {
        console.log(err);
        reject(err);
      },
      configs: [
        {
          type: "RANGE_DELETE",
          parentID: viewer.id,
          connectionKeys: [{ key: "CategoryList_data" }],
          pathToConnection: ["viewer", "data"],
          deletedIDFieldName: ["node", "id"],
        },
        {
          type: "RANGE_DELETE",
          parentID: viewer.id,
          connectionKeys: [{ key: "Form_categories" }],
          pathToConnection: ["viewer", "categories"],
          deletedIDFieldName: ["node", "id"],
        },
      ],
    })
  );
}

export default { commit };
