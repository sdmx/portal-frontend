/* tslint:disable */
/* eslint-disable */
/* @relayHash 213c38a326c6fbf7d5f6eb8d89410fdb */

import { ConcreteRequest } from "relay-runtime";
export type DeleteCategoryMutationVariables = {
    id: string;
};
export type DeleteCategoryMutationResponse = {
    readonly deleteCategory: {
        readonly node: {
            readonly id: string;
        } | null;
    } | null;
};
export type DeleteCategoryMutation = {
    readonly response: DeleteCategoryMutationResponse;
    readonly variables: DeleteCategoryMutationVariables;
};



/*
mutation DeleteCategoryMutation(
  $id: ID!
) {
  deleteCategory(id: $id) {
    node {
      id
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "deleteCategory",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "id",
        "variableName": "id"
      }
    ],
    "concreteType": "CategoryPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "node",
        "storageKey": null,
        "args": null,
        "concreteType": "Category",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "DeleteCategoryMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "DeleteCategoryMutation",
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "params": {
    "operationKind": "mutation",
    "name": "DeleteCategoryMutation",
    "id": null,
    "text": "mutation DeleteCategoryMutation(\n  $id: ID!\n) {\n  deleteCategory(id: $id) {\n    node {\n      id\n    }\n  }\n}\n",
    "metadata": {}
  }
};
})();
(node as any).hash = 'db78375cd69d879f92bcf7f752f2f562';
export default node;
