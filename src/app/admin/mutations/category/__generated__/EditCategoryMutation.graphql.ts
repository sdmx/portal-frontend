/* tslint:disable */
/* eslint-disable */
/* @relayHash f15bea7b9bd2f99a21bf481f136049ef */

import { ConcreteRequest } from "relay-runtime";
export type CategoryInput = {
    name: string;
    parentId?: string | null;
};
export type EditCategoryMutationVariables = {
    id: string;
    input: CategoryInput;
};
export type EditCategoryMutationResponse = {
    readonly updateCategory: {
        readonly node: {
            readonly id: string;
        } | null;
    } | null;
};
export type EditCategoryMutation = {
    readonly response: EditCategoryMutationResponse;
    readonly variables: EditCategoryMutationVariables;
};



/*
mutation EditCategoryMutation(
  $id: ID!
  $input: CategoryInput!
) {
  updateCategory(id: $id, input: $input) {
    node {
      id
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CategoryInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "updateCategory",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "id",
        "variableName": "id"
      },
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "CategoryPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "node",
        "storageKey": null,
        "args": null,
        "concreteType": "Category",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "EditCategoryMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "EditCategoryMutation",
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "params": {
    "operationKind": "mutation",
    "name": "EditCategoryMutation",
    "id": null,
    "text": "mutation EditCategoryMutation(\n  $id: ID!\n  $input: CategoryInput!\n) {\n  updateCategory(id: $id, input: $input) {\n    node {\n      id\n    }\n  }\n}\n",
    "metadata": {}
  }
};
})();
(node as any).hash = '3e76df124352783c1b0845dc6637f206';
export default node;
