/* tslint:disable */
/* eslint-disable */
/* @relayHash cf44ddddc81811ca853771761f9fd506 */

import { ConcreteRequest } from "relay-runtime";
export type CategoryInput = {
    name: string;
    parentId?: string | null;
};
export type CreateCategoryMutationVariables = {
    input: CategoryInput;
};
export type CreateCategoryMutationResponse = {
    readonly createCategory: {
        readonly node: {
            readonly id: string;
            readonly name: string;
        } | null;
    } | null;
};
export type CreateCategoryMutation = {
    readonly response: CreateCategoryMutationResponse;
    readonly variables: CreateCategoryMutationVariables;
};



/*
mutation CreateCategoryMutation(
  $input: CategoryInput!
) {
  createCategory(input: $input) {
    node {
      id
      name
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "CategoryInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "createCategory",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "CategoryPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "node",
        "storageKey": null,
        "args": null,
        "concreteType": "Category",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "id",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "name",
            "args": null,
            "storageKey": null
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "CreateCategoryMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateCategoryMutation",
    "argumentDefinitions": (v0/*: any*/),
    "selections": (v1/*: any*/)
  },
  "params": {
    "operationKind": "mutation",
    "name": "CreateCategoryMutation",
    "id": null,
    "text": "mutation CreateCategoryMutation(\n  $input: CategoryInput!\n) {\n  createCategory(input: $input) {\n    node {\n      id\n      name\n    }\n  }\n}\n",
    "metadata": {}
  }
};
})();
(node as any).hash = 'a9aaf38231e89ee0e7814a9392f12388';
export default node;
