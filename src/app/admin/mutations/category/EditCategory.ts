import { commitMutation, graphql } from "react-relay";
import { Environment } from "relay-runtime";
import { CategoryInput } from 'types/schema';

function commit(environment: Environment, input: CategoryInput, id: string) {
  return new Promise((resolve, reject) => {
    return commitMutation(environment, {
      mutation: graphql`
        mutation EditCategoryMutation($id: ID!, $input: CategoryInput!) {
          updateCategory(id: $id, input: $input) {
            node {
              id
            }
          }
        }
      `,
      variables: { input, id },
      onCompleted: resolve,
      onError: (err) => {
        console.log(err);
        reject(err);
      },
    });
  });
}

export default { commit };
