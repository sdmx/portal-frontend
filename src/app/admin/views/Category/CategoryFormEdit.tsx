import React from "react";
import { graphql, fetchQuery } from "react-relay";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { injectIntl, IntlShape } from "react-intl";
import { Col, PageHeader, message } from "antd";

import environment from "relayEnvironment";
import Content from "components/Content";
import EditCategory from "app/admin/mutations/category/EditCategory";
import Form from "app/admin/views/Category/CategoryForm";
import { Viewer } from 'types/schema';

interface Props extends RouteComponentProps<{ id: string }> {
  intl: IntlShape;
  viewer: Viewer;
  retry: any;
}

class CategoryFormEdit extends React.Component<Props> {
  state = { initialValues: null };

  componentDidMount() {
    const { match } = this.props;
    const { id } = match.params;

    const query = graphql`
      query CategoryFormEditQuery($id: ID!) {
        viewer {
          category(id: $id) {
            name
            parent {
              id
            }
          }
        }
      }
    `;

    fetchQuery(environment, query, { id }).then(
      ({ viewer: { category } }: any) => {
        const initialValues: any = {};
        console.log(category);

        for (const key in category) {
          if (category[key]) {
            switch (key) {
              case "parent":
                initialValues.parentId = category[key].id;
                break;

              default:
                initialValues[key] = category[key];
                break;
            }
          }
        }

        console.log(initialValues);
        this.setState({ initialValues });
      }
    );
  }

  render() {
    const { history, intl, viewer, retry, match } = this.props;
    const { initialValues } = this.state;

    return (
      <div>
        <PageHeader
          title={intl.formatMessage({ id: "app.category.edit" })}
          onBack={() => history.push("/admin/category")}
        />

        <Content>
          <Col span={20}>
            {initialValues && (
              <Form
                viewer={viewer}
                initialValues={initialValues}
                handleSubmit={(values) => {
                  EditCategory.commit(environment, values, match.params.id)
                    .then(() => {
                      retry();
                      history.push("/admin/category");
                      message.success(
                        intl.formatMessage({ id: "info.save.success" })
                      );
                    })
                    .catch(() => {
                      message.error(
                        intl.formatMessage({ id: "info.save.error" })
                      );
                    });
                }}
              />
            )}
          </Col>
        </Content>
      </div>
    );
  }
}

export default withRouter(injectIntl(CategoryFormEdit));
