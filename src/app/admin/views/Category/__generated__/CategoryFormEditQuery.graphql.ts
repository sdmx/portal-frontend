/* tslint:disable */
/* eslint-disable */
/* @relayHash 82e57340258646cc72a3ef7bbb7a607b */

import { ConcreteRequest } from "relay-runtime";
export type CategoryFormEditQueryVariables = {
    id: string;
};
export type CategoryFormEditQueryResponse = {
    readonly viewer: {
        readonly category: {
            readonly name: string;
            readonly parent: {
                readonly id: string;
            } | null;
        } | null;
    };
};
export type CategoryFormEditQuery = {
    readonly response: CategoryFormEditQueryResponse;
    readonly variables: CategoryFormEditQueryVariables;
};



/*
query CategoryFormEditQuery(
  $id: ID!
) {
  viewer {
    category(id: $id) {
      name
      parent {
        id
      }
      id
    }
    id
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "name",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "parent",
  "storageKey": null,
  "args": null,
  "concreteType": "Category",
  "plural": false,
  "selections": [
    (v3/*: any*/)
  ]
};
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "CategoryFormEditQuery",
    "type": "RootQuery",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "viewer",
        "storageKey": null,
        "args": null,
        "concreteType": "Viewer",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "category",
            "storageKey": null,
            "args": (v1/*: any*/),
            "concreteType": "Category",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v4/*: any*/)
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "CategoryFormEditQuery",
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "viewer",
        "storageKey": null,
        "args": null,
        "concreteType": "Viewer",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "category",
            "storageKey": null,
            "args": (v1/*: any*/),
            "concreteType": "Category",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v4/*: any*/),
              (v3/*: any*/)
            ]
          },
          (v3/*: any*/)
        ]
      }
    ]
  },
  "params": {
    "operationKind": "query",
    "name": "CategoryFormEditQuery",
    "id": null,
    "text": "query CategoryFormEditQuery(\n  $id: ID!\n) {\n  viewer {\n    category(id: $id) {\n      name\n      parent {\n        id\n      }\n      id\n    }\n    id\n  }\n}\n",
    "metadata": {}
  }
};
})();
(node as any).hash = 'b2d6ab3ecddda7d4d7925885c4542911';
export default node;
