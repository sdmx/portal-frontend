/* tslint:disable */
/* eslint-disable */
/* @relayHash fe88ca3a81adca0f4bec618f046c3806 */

import { ConcreteRequest } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type Direction = "ASC" | "DESC" | "%future added value";
export type Order = {
    by: string;
    direction?: Direction | null;
};
export type CategoryQueryVariables = {
    filter?: string | null;
    first?: number | null;
    offset?: number | null;
    order?: Order | null;
    withTotalCount: boolean;
};
export type CategoryQueryResponse = {
    readonly viewer: {
        readonly id: string;
        readonly " $fragmentRefs": FragmentRefs<"CategoryForm_viewer" | "CategoryList_viewer">;
    };
};
export type CategoryQuery = {
    readonly response: CategoryQueryResponse;
    readonly variables: CategoryQueryVariables;
};



/*
query CategoryQuery(
  $filter: String
  $first: Int
  $offset: Int
  $order: Order
  $withTotalCount: Boolean!
) {
  viewer {
    id
    ...CategoryForm_viewer
    ...CategoryList_viewer_2A7R4U
  }
}

fragment CategoryForm_viewer on Viewer {
  id
  categories: allCategories(order: {by: "name"}) {
    edges {
      node {
        ... on Category {
          id
          name
        }
        id
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}

fragment CategoryList_viewer_2A7R4U on Viewer {
  id
  data: allCategories(filter: $filter, first: $first, offset: $offset, order: $order) {
    totalCount @include(if: $withTotalCount)
    edges {
      node {
        ... on Category {
          id
          name
          parent {
            name
            id
          }
        }
        id
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "filter",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "offset",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "order",
    "type": "Order",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "withTotalCount",
    "type": "Boolean!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "Variable",
  "name": "filter",
  "variableName": "filter"
},
v3 = {
  "kind": "Variable",
  "name": "first",
  "variableName": "first"
},
v4 = {
  "kind": "Variable",
  "name": "offset",
  "variableName": "offset"
},
v5 = {
  "kind": "Variable",
  "name": "order",
  "variableName": "order"
},
v6 = [
  {
    "kind": "Literal",
    "name": "order",
    "value": {
      "by": "name"
    }
  }
],
v7 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v8 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "name",
  "args": null,
  "storageKey": null
},
v9 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "cursor",
  "args": null,
  "storageKey": null
},
v10 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "pageInfo",
  "storageKey": null,
  "args": null,
  "concreteType": "PageInfo",
  "plural": false,
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "endCursor",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "hasNextPage",
      "args": null,
      "storageKey": null
    }
  ]
},
v11 = [
  (v2/*: any*/),
  (v3/*: any*/),
  (v4/*: any*/),
  (v5/*: any*/)
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "CategoryQuery",
    "type": "RootQuery",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "viewer",
        "storageKey": null,
        "args": null,
        "concreteType": "Viewer",
        "plural": false,
        "selections": [
          (v1/*: any*/),
          {
            "kind": "FragmentSpread",
            "name": "CategoryForm_viewer",
            "args": null
          },
          {
            "kind": "FragmentSpread",
            "name": "CategoryList_viewer",
            "args": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              {
                "kind": "Variable",
                "name": "withTotalCount",
                "variableName": "withTotalCount"
              }
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "CategoryQuery",
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "viewer",
        "storageKey": null,
        "args": null,
        "concreteType": "Viewer",
        "plural": false,
        "selections": [
          (v1/*: any*/),
          {
            "kind": "LinkedField",
            "alias": "categories",
            "name": "allCategories",
            "storageKey": "allCategories(order:{\"by\":\"name\"})",
            "args": (v6/*: any*/),
            "concreteType": "Connection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "Edge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": null,
                    "plural": false,
                    "selections": [
                      (v1/*: any*/),
                      (v7/*: any*/),
                      {
                        "kind": "InlineFragment",
                        "type": "Category",
                        "selections": [
                          (v8/*: any*/)
                        ]
                      }
                    ]
                  },
                  (v9/*: any*/)
                ]
              },
              (v10/*: any*/)
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": "categories",
            "name": "allCategories",
            "args": (v6/*: any*/),
            "handle": "connection",
            "key": "Form_categories",
            "filters": []
          },
          {
            "kind": "LinkedField",
            "alias": "data",
            "name": "allCategories",
            "storageKey": null,
            "args": (v11/*: any*/),
            "concreteType": "Connection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "Edge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": null,
                    "plural": false,
                    "selections": [
                      (v1/*: any*/),
                      (v7/*: any*/),
                      {
                        "kind": "InlineFragment",
                        "type": "Category",
                        "selections": [
                          (v8/*: any*/),
                          {
                            "kind": "LinkedField",
                            "alias": null,
                            "name": "parent",
                            "storageKey": null,
                            "args": null,
                            "concreteType": "Category",
                            "plural": false,
                            "selections": [
                              (v8/*: any*/),
                              (v1/*: any*/)
                            ]
                          }
                        ]
                      }
                    ]
                  },
                  (v9/*: any*/)
                ]
              },
              (v10/*: any*/),
              {
                "kind": "Condition",
                "passingValue": true,
                "condition": "withTotalCount",
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "totalCount",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": "data",
            "name": "allCategories",
            "args": (v11/*: any*/),
            "handle": "connection",
            "key": "CategoryList_data",
            "filters": []
          }
        ]
      }
    ]
  },
  "params": {
    "operationKind": "query",
    "name": "CategoryQuery",
    "id": null,
    "text": "query CategoryQuery(\n  $filter: String\n  $first: Int\n  $offset: Int\n  $order: Order\n  $withTotalCount: Boolean!\n) {\n  viewer {\n    id\n    ...CategoryForm_viewer\n    ...CategoryList_viewer_2A7R4U\n  }\n}\n\nfragment CategoryForm_viewer on Viewer {\n  id\n  categories: allCategories(order: {by: \"name\"}) {\n    edges {\n      node {\n        ... on Category {\n          id\n          name\n        }\n        id\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n\nfragment CategoryList_viewer_2A7R4U on Viewer {\n  id\n  data: allCategories(filter: $filter, first: $first, offset: $offset, order: $order) {\n    totalCount @include(if: $withTotalCount)\n    edges {\n      node {\n        ... on Category {\n          id\n          name\n          parent {\n            name\n            id\n          }\n        }\n        id\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
    "metadata": {}
  }
};
})();
(node as any).hash = '09ce8f467eee01322806fa12b2a59c7a';
export default node;
