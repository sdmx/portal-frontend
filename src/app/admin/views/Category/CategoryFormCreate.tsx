import React from "react";
import { Col, PageHeader, message } from "antd";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { injectIntl, IntlShape } from "react-intl";

import environment from "relayEnvironment";
import Content from "components/Content";
import CreateCategory from "app/admin/mutations/category/CreateCategory";
import Form from "app/admin/views/Category/CategoryForm";
import { Viewer } from 'types/schema';

interface Props extends RouteComponentProps {
  intl: IntlShape;
  viewer: Viewer;
  retry: any;
}

class CategoryFormCreate extends React.Component<Props> {
  render() {
    const { history, intl, viewer, retry } = this.props;
    const fmt = intl.formatMessage;

    return (
      <div>
        <PageHeader
          title={fmt({ id: "app.category.create" })}
          onBack={() => history.push("/admin/category")}
        />

        <Content>
          <Col span={16}>
            <Form
              viewer={viewer}
              handleSubmit={(values) => {
                CreateCategory.commit(environment, viewer, values)
                  .then(() => {
                    // retry();
                    history.push("/admin/category");
                    message.success(fmt({ id: "info.save.success" }));
                  })
                  .catch(() => {
                    message.error(fmt({ id: "info.save.error" }));
                  });
              }}
            />
          </Col>
        </Content>
      </div>
    );
  }
}

export default withRouter(injectIntl(CategoryFormCreate));
