import React from "react";
import { injectIntl, IntlShape } from "react-intl";
import { Button, Form as AntForm, Input, Select } from "antd";
import { createFragmentContainer, graphql } from "react-relay";
import { Viewer, Maybe, Connection } from 'types/schema';

export interface Props {
  nodeId?: string;
  intl: IntlShape;
  viewer: Viewer & { categories?: Maybe<Connection> };
  initialValues?: any;
  handleSubmit: (values: any) => any;
}

class CategoryForm extends React.Component<Props> {
  render() {
    const { nodeId, initialValues, handleSubmit, intl, viewer } = this.props;
    const fmt = intl.formatMessage;

    return (
      <AntForm
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={initialValues}
        onFinish={(values) => handleSubmit(values)}
      >
        <AntForm.Item
          name="name"
          label={fmt({ id: "app.category.name" })}
          rules={[{ required: true }]}
        >
          <Input />
        </AntForm.Item>

        <AntForm.Item
          name="parentId"
          label={fmt({ id: "app.category.parent" })}
        >
          <Select
            showSearch
            placeholder={fmt({ id: "app.category.parent" })}
            filterOption={(input, option: any) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {viewer.categories?.edges?.map(
              ({ node }: any) =>
                nodeId !== node.id && (
                  <Select.Option key={node.id} value={node.id}>
                    {node.name}
                  </Select.Option>
                )
            )}
          </Select>
        </AntForm.Item>

        <AntForm.Item wrapperCol={{ span: 16, offset: 8 }}>
          <Button type="primary" htmlType="submit">
            {fmt({ id: "action.submit" })}
          </Button>
        </AntForm.Item>
      </AntForm>
    );
  }
}

export default createFragmentContainer(injectIntl(CategoryForm), {
  viewer: graphql`
    fragment CategoryForm_viewer on Viewer {
      id
      categories: allCategories(first: null, order: { by: "name" })
        @connection(key: "Form_categories", filters: []) {
        edges {
          node {
            ... on Category {
              id
              name
            }
          }
        }
      }
    }
  `,
});
