import React from "react";
import { Col, Input, Row, Space, message } from "antd";
import { IntlShape, injectIntl } from "react-intl";
import { graphql, createPaginationContainer } from "react-relay";

import environment from "relayEnvironment";
import { Viewer, Maybe, Connection } from "types/schema";

import CreateButton from "components/Button/CreateButton";
import DeleteButton from "components/Button/DeleteButton";
import EditButton from "components/Button/EditButton";
import Content from "components/Content";

import DeleteCategory from "app/admin/mutations/category/DeleteCategory";
import TableData from "app/admin/components/TableData";

interface Props {
  intl: IntlShape;
  viewer: Viewer & { data?: Maybe<Connection> };
  relay: any;
}

const pageSize = 10;

class CategoryList extends React.Component<Props> {
  state = { filter: null, totalCount: 0, isLoading: false, variables: {} };

  componentDidMount() {
    const { viewer } = this.props;

    this.setState({
      totalCount: viewer.data?.totalCount,
    });
  }

  filter(filter: string) {
    const { relay, viewer } = this.props;
    this.setState({ isLoading: true });

    const callback = (err?: Error) => {
      this.setState({
        filter,
        isLoading: false,
        totalCount: viewer.data?.totalCount,
      });
    };

    relay.refetchConnection(pageSize, callback, {
      filter,
      withTotalCount: true,
    });
  }

  fetch(variables: any = {}) {
    this.setState({ isLoading: true, variables });
    this.props.relay.refetchConnection(
      pageSize,
      () => this.setState({ isLoading: false }),
      variables
    );
  }

  delete(viewer: any, id: string) {
    const { intl } = this.props;

    DeleteCategory.commit(environment, viewer, id)
      .then(() => {
        message.success(intl.formatMessage({ id: "info.delete.success" }));
      })
      .catch(() => {
        message.error(intl.formatMessage({ id: "info.delete.error" }));
      });
  }

  render() {
    const { isLoading, totalCount } = this.state;
    const { intl, viewer } = this.props;
    const fmt = intl.formatMessage;

    return (
      <Content>
        <Row>
          <Col span={16}>
            <CreateButton url="/admin/category/create" />
          </Col>
          <Col span={8}>
            <Input.Search
              enterButton
              allowClear
              placeholder={fmt({ id: "action.search" })}
              onSearch={(query) => this.filter(query)}
            />
          </Col>
        </Row>
        <br />

        <TableData
          data={viewer.data}
          pageSize={pageSize}
          isLoading={isLoading}
          totalCount={totalCount}
          onFetch={(variables) => this.fetch(variables)}
          columns={[
            {
              key: "name",
              title: fmt({ id: "app.category.name" }),
              dataIndex: ["node", "name"],
              sorter: true,
            },
            {
              key: "parent",
              title: fmt({ id: "app.category.parent" }),
              dataIndex: ["node", "parent", "name"],
              sorter: true,
            },
            {
              key: "action",
              title: fmt({ id: "app.action" }),
              width: 200,
              render: ({ node }: any) => (
                <Space key={`act/${node?.id}`}>
                  <EditButton url={`/admin/category/edit/${node?.id}`} />
                  <DeleteButton
                    onConfirm={() => this.delete(viewer, node?.id)}
                  />
                </Space>
              ),
            },
          ]}
        />
      </Content>
    );
  }
}

export default createPaginationContainer(
  injectIntl(CategoryList),
  {
    viewer: graphql`
      fragment CategoryList_viewer on Viewer
        @argumentDefinitions(
          filter: { type: "String" }
          first: { type: "Int" }
          offset: { type: "Int" }
          order: { type: "Order" }
          withTotalCount: { type: "Boolean!", defaultValue: false }
        ) {
        id
        data: allCategories(
          filter: $filter
          first: $first
          offset: $offset
          order: $order
        ) @connection(key: "CategoryList_data", filters: []) {
          totalCount @include(if: $withTotalCount)
          edges {
            node {
              ... on Category {
                id
                name
                parent {
                  name
                }
              }
            }
          }
        }
      }
    `,
  },
  {
    direction: "forward",
    getConnectionFromProps(props) {
      return props.viewer && props.viewer.data;
    },
    getVariables(props, { count }, variables) {
      return {
        count,
        ...variables,
        withTotalCount: false,
      };
    },
    query: graphql`
      query CategoryListRefetchQuery(
        $filter: String
        $first: Int
        $offset: Int
        $order: Order
        $withTotalCount: Boolean!
      ) {
        viewer {
          ...CategoryList_viewer
            @arguments(
              filter: $filter
              first: $first
              offset: $offset
              order: $order
              withTotalCount: $withTotalCount
            )
        }
      }
    `,
  }
);
