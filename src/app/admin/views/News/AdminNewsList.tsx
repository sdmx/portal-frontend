import React from "react";
// import moment from "moment";
import { graphql, createPaginationContainer, RelayPaginationProp } from "react-relay";
import { IntlShape, injectIntl } from "react-intl";
import { Col, Input, Row, Space, message } from "antd";

import { Maybe, Viewer, Connection } from "types/schema";
import environment from "relayEnvironment";
import CreateButton from "components/Button/CreateButton";
import DeleteButton from "components/Button/DeleteButton";
import EditButton from "components/Button/EditButton";
import DeleteNews from "app/admin/mutations/news/DeleteNews";
import TableData from "app/admin/components/TableData";
import Content from "components/Content";
// import StatusLabel from "app/admin/components/StatusLabel";
// import BooleanLabel from "components/BooleanLabel";

interface Props {
  intl: IntlShape;
  relay: RelayPaginationProp;
  viewer: Viewer & { data?: Maybe<Connection> };
}

const pageSize = 10;

class AdminNewsList extends React.Component<Props> {
  state = { filter: null, totalCount: 0, isLoading: false, variables: {} };

  componentDidMount() {
    this.setState({
      totalCount: this.props.viewer?.data?.totalCount,
    });
  }

  filter(filter: string) {
    this.setState({ isLoading: true });

    const callback = () => {
      this.setState({
        filter,
        isLoading: false,
        totalCount: this.props.viewer?.data?.totalCount,
      });
    };

    this.props.relay.refetchConnection(pageSize, callback, {
      filter,
      withTotalCount: true,
    });
  }

  fetch(variables: any = {}) {
    this.setState({ isLoading: true, variables });
    this.props.relay.refetchConnection(
      pageSize,
      () => this.setState({ isLoading: false }),
      variables
    );
  }

  delete(id: string) {
    const { intl } = this.props;
    const { filter, variables } = this.state;

    DeleteNews.commit(environment, id)
      .then(() => {
        // FIXME: show empty result if there is only one data left on the last page
        this.fetch({ ...variables, filter, withTotalCount: true });
        message.success(intl.formatMessage({ id: "info.delete.success" }));
      })
      .catch(() => {
        message.error(intl.formatMessage({ id: "info.delete.error" }));
      });
  }

  render() {
    const { isLoading, totalCount } = this.state;
    const { intl, viewer } = this.props;
    const fmt = intl.formatMessage;
    // console.log(viewer.data?.edges.length);

    return (
      <Content>
        <Row>
          <Col span={16}>
            <CreateButton url={`/admin/news/create`} />
          </Col>
          <Col span={8}>
            <Input.Search
              enterButton
              allowClear
              placeholder={fmt({ id: "action.search" })}
              onSearch={(query) => this.filter(query)}
            />
          </Col>
        </Row>
        <br />

        <TableData
          data={viewer.data}
          pageSize={pageSize}
          isLoading={isLoading}
          totalCount={totalCount}
          onFetch={(variables) => this.fetch(variables)}
          columns={[
            {
              key: "name",
              title: fmt({ id: "app.news.name" }),
              dataIndex: ["node", "name"],
              sorter: true,
              ellipsis: true,
            },
            // {
            //   key: "created",
            //   title: fmt({ id: "app.created" }),
            //   dataIndex: ["node", "created"],
            //   sorter: true,
            //   render: (value: any) => moment(value).fromNow(),
            // },
            // {
            //   key: "updated",
            //   title: fmt({ id: "app.updated" }),
            //   dataIndex: ["node", "updated"],
            //   sorter: true,
            //   render: (value: any) => moment(value).fromNow(),
            // },
            // {
            //   key: "status",
            //   title: fmt({ id: "app.status" }),
            //   dataIndex: ["node", "status"],
            //   sorter: true,
            //   render: (value: any) => <StatusLabel value={value} />,
            // },
            // {
            //   key: "isPublished",
            //   title: fmt({ id: "app.news.isPublished" }),
            //   dataIndex: ["node", "isPublished"],
            //   // FIXME: sorting failed
            //   sorter: true,
            //   render: (value: any) => <BooleanLabel value={value} />,
            // },
            {
              key: "action",
              title: fmt({ id: "app.action" }),
              width: 200,
              render: ({ node }: any) => (
                <Space key={`act/${node?.id}`}>
                  <EditButton url={`/admin/news/edit/${node?.id}`} />
                  <DeleteButton onConfirm={() => this.delete(node?.id)} />
                </Space>
              ),
            },
          ]}
        />
      </Content>
    );
  }
}

export default createPaginationContainer(
  injectIntl(AdminNewsList),
  {
    viewer: graphql`
      fragment AdminNewsList_viewer on Viewer
        @argumentDefinitions(
          filter: { type: "String" }
          first: { type: "Int" }
          offset: { type: "Int" }
          order: { type: "Order" }
          withTotalCount: { type: "Boolean!", defaultValue: false }
        ) {
        id
        data: allNews(
          filter: $filter
          first: $first
          offset: $offset
          order: $order
        ) @connection(key: "NewsList_data", filters: []) {
          totalCount @include(if: $withTotalCount)
          edges {
            node {
              ... on News {
                id
                name
                status
                isPublished
                created
                updated
              }
            }
          }
        }
      }
    `,
  },
  {
    direction: "forward",
    getConnectionFromProps(props) {
      return props.viewer && props.viewer.data;
    },
    getVariables(props, { count }, variables) {
      return {
        count,
        ...variables,
        withTotalCount: false,
      };
    },
    query: graphql`
      query AdminNewsListRefetchQuery(
        $filter: String
        $first: Int
        $offset: Int
        $order: Order
        $withTotalCount: Boolean!
      ) {
        viewer {
          ...AdminNewsList_viewer
            @arguments(
              filter: $filter
              first: $first
              offset: $offset
              order: $order
              withTotalCount: $withTotalCount
            )
        }
      }
    `,
  }
);
