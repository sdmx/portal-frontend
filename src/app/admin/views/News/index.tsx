import React from "react";
import { FormattedMessage } from "react-intl";
import { graphql, QueryRenderer } from "react-relay";
import { Route, Switch } from "react-router-dom";

import environment from "relayEnvironment";
import { Viewer } from "types/schema";
import Error from "components/Error";

const FormCreate = React.lazy(() => import("./AdminNewsFormCreate"));
const FormEdit = React.lazy(() => import("./AdminNewsFormEdit"));
const List = React.lazy(() => import("./AdminNewsList"));

class News extends React.Component {
  render() {
    return (
      <QueryRenderer<{ variables: any; response: { viewer: Viewer } }>
        environment={environment}
        variables={{ withTotalCount: true }}
        render={({ error, props, retry }) => {
          if (error) {
            return <Error message={error.message} />;
          } else if (props) {
            return (
              <Switch>
                <Route exact path="/admin/news">
                  <List viewer={props.viewer} />
                </Route>
                <Route path="/admin/news/create">
                  <FormCreate retry={retry} viewer={props.viewer} />
                </Route>
                <Route path="/admin/news/edit/:id">
                  <FormEdit retry={retry} viewer={props.viewer} />
                </Route>
                <Route path="/admin/news*">
                  <Error message={<FormattedMessage id="error.404" />} />
                </Route>
              </Switch>
            );
          }

          // return <Loading />;
        }}
        query={graphql`
          query NewsQuery(
            $filter: String
            $first: Int
            $offset: Int
            $order: Order
            $withTotalCount: Boolean!
          ) {
            viewer {
              id
              ...AdminNewsForm_viewer
              ...AdminNewsList_viewer
                @arguments(
                  filter: $filter
                  first: $first
                  offset: $offset
                  order: $order
                  withTotalCount: $withTotalCount
                )
            }
          }
        `}
      />
    );
  }
}

export default News;
