import React from "react";
import { Col, PageHeader, message } from "antd";
import { injectIntl, IntlShape } from "react-intl";
import { RouteComponentProps, withRouter } from "react-router-dom";

import environment from "relayEnvironment";
import { Viewer } from "types/schema";

import Content from "components/Content";

import CreateNews from "app/admin/mutations/news/CreateNews";
import AdminNewsForm from "app/admin/views/News/AdminNewsForm";

interface Props extends RouteComponentProps {
  intl: IntlShape;
  viewer: Viewer;
  retry: (() => void) | null;
}

class AdminNewsFormCreate extends React.Component<Props> {
  render() {
    const { history, intl, viewer, retry } = this.props;

    return (
      <div>
        <PageHeader
          title={intl.formatMessage({ id: "app.news.create" })}
          onBack={() => history.push("/admin/news")}
        />

        <Content>
          <Col span={20}>
            <AdminNewsForm
              viewer={viewer}
              handleSubmit={({ thumbnail, ...values }) => {
                CreateNews.commit(environment, values, { thumbnail })
                  .then(() => {
                    retry && retry();
                    history.push("/admin/news");
                    message.success(
                      intl.formatMessage({ id: "info.save.success" })
                    );
                  })
                  .catch(() => {
                    message.error(
                      intl.formatMessage({ id: "info.save.error" })
                    );
                  });
              }}
            />
          </Col>
        </Content>
      </div>
    );
  }
}

export default withRouter(injectIntl(AdminNewsFormCreate));
