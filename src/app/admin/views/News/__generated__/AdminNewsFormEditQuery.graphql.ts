/* tslint:disable */
/* eslint-disable */
/* @relayHash a30a2a805b8a3f36613d3ca38d8d2201 */

import { ConcreteRequest } from "relay-runtime";
export type AdminNewsFormEditQueryVariables = {
    id: string;
};
export type AdminNewsFormEditQueryResponse = {
    readonly viewer: {
        readonly news: {
            readonly name: string;
            readonly body: string | null;
            readonly links: ReadonlyArray<string | null> | null;
            readonly thumbnail: unknown | null;
            readonly categories: ReadonlyArray<{
                readonly id: string;
            } | null> | null;
        } | null;
    };
};
export type AdminNewsFormEditQuery = {
    readonly response: AdminNewsFormEditQueryResponse;
    readonly variables: AdminNewsFormEditQueryVariables;
};



/*
query AdminNewsFormEditQuery(
  $id: ID!
) {
  viewer {
    news(id: $id) {
      name
      body
      links
      thumbnail
      categories {
        id
      }
      id
    }
    id
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "name",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "body",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "links",
  "args": null,
  "storageKey": null
},
v5 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "thumbnail",
  "args": null,
  "storageKey": null
},
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v7 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "categories",
  "storageKey": null,
  "args": null,
  "concreteType": "Category",
  "plural": true,
  "selections": [
    (v6/*: any*/)
  ]
};
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "AdminNewsFormEditQuery",
    "type": "RootQuery",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "viewer",
        "storageKey": null,
        "args": null,
        "concreteType": "Viewer",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "news",
            "storageKey": null,
            "args": (v1/*: any*/),
            "concreteType": "News",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              (v7/*: any*/)
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "AdminNewsFormEditQuery",
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "viewer",
        "storageKey": null,
        "args": null,
        "concreteType": "Viewer",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "news",
            "storageKey": null,
            "args": (v1/*: any*/),
            "concreteType": "News",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              (v7/*: any*/),
              (v6/*: any*/)
            ]
          },
          (v6/*: any*/)
        ]
      }
    ]
  },
  "params": {
    "operationKind": "query",
    "name": "AdminNewsFormEditQuery",
    "id": null,
    "text": "query AdminNewsFormEditQuery(\n  $id: ID!\n) {\n  viewer {\n    news(id: $id) {\n      name\n      body\n      links\n      thumbnail\n      categories {\n        id\n      }\n      id\n    }\n    id\n  }\n}\n",
    "metadata": {}
  }
};
})();
(node as any).hash = 'f531854f58b5e65cee88a778bb06f340';
export default node;
