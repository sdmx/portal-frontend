/* tslint:disable */
/* eslint-disable */
/* @relayHash b73abd72787272f3e63d56b31638c7ed */

import { ConcreteRequest } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type Direction = "ASC" | "DESC" | "%future added value";
export type Order = {
    by: string;
    direction?: Direction | null;
};
export type NewsQueryVariables = {
    filter?: string | null;
    first?: number | null;
    offset?: number | null;
    order?: Order | null;
    withTotalCount: boolean;
};
export type NewsQueryResponse = {
    readonly viewer: {
        readonly id: string;
        readonly " $fragmentRefs": FragmentRefs<"AdminNewsForm_viewer" | "AdminNewsList_viewer">;
    };
};
export type NewsQuery = {
    readonly response: NewsQueryResponse;
    readonly variables: NewsQueryVariables;
};



/*
query NewsQuery(
  $filter: String
  $first: Int
  $offset: Int
  $order: Order
  $withTotalCount: Boolean!
) {
  viewer {
    id
    ...AdminNewsForm_viewer
    ...AdminNewsList_viewer_2A7R4U
  }
}

fragment AdminNewsForm_viewer on Viewer {
  categories: allCategories(order: {by: "name"}) {
    edges {
      node {
        __typename
        ... on Category {
          id
          name
        }
        id
      }
    }
  }
}

fragment AdminNewsList_viewer_2A7R4U on Viewer {
  id
  data: allNews(filter: $filter, first: $first, offset: $offset, order: $order) {
    totalCount @include(if: $withTotalCount)
    edges {
      node {
        ... on News {
          id
          name
          status
          isPublished
          created
          updated
        }
        id
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      hasNextPage
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "filter",
    "type": "String",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "offset",
    "type": "Int",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "order",
    "type": "Order",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "withTotalCount",
    "type": "Boolean!",
    "defaultValue": null
  }
],
v1 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v2 = {
  "kind": "Variable",
  "name": "filter",
  "variableName": "filter"
},
v3 = {
  "kind": "Variable",
  "name": "first",
  "variableName": "first"
},
v4 = {
  "kind": "Variable",
  "name": "offset",
  "variableName": "offset"
},
v5 = {
  "kind": "Variable",
  "name": "order",
  "variableName": "order"
},
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "__typename",
  "args": null,
  "storageKey": null
},
v7 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "name",
  "args": null,
  "storageKey": null
},
v8 = [
  (v2/*: any*/),
  (v3/*: any*/),
  (v4/*: any*/),
  (v5/*: any*/)
];
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "NewsQuery",
    "type": "RootQuery",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "viewer",
        "storageKey": null,
        "args": null,
        "concreteType": "Viewer",
        "plural": false,
        "selections": [
          (v1/*: any*/),
          {
            "kind": "FragmentSpread",
            "name": "AdminNewsForm_viewer",
            "args": null
          },
          {
            "kind": "FragmentSpread",
            "name": "AdminNewsList_viewer",
            "args": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              {
                "kind": "Variable",
                "name": "withTotalCount",
                "variableName": "withTotalCount"
              }
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "NewsQuery",
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "viewer",
        "storageKey": null,
        "args": null,
        "concreteType": "Viewer",
        "plural": false,
        "selections": [
          (v1/*: any*/),
          {
            "kind": "LinkedField",
            "alias": "categories",
            "name": "allCategories",
            "storageKey": "allCategories(order:{\"by\":\"name\"})",
            "args": [
              {
                "kind": "Literal",
                "name": "order",
                "value": {
                  "by": "name"
                }
              }
            ],
            "concreteType": "Connection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "Edge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": null,
                    "plural": false,
                    "selections": [
                      (v6/*: any*/),
                      (v1/*: any*/),
                      {
                        "kind": "InlineFragment",
                        "type": "Category",
                        "selections": [
                          (v7/*: any*/)
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedField",
            "alias": "data",
            "name": "allNews",
            "storageKey": null,
            "args": (v8/*: any*/),
            "concreteType": "Connection",
            "plural": false,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "edges",
                "storageKey": null,
                "args": null,
                "concreteType": "Edge",
                "plural": true,
                "selections": [
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "node",
                    "storageKey": null,
                    "args": null,
                    "concreteType": null,
                    "plural": false,
                    "selections": [
                      (v1/*: any*/),
                      (v6/*: any*/),
                      {
                        "kind": "InlineFragment",
                        "type": "News",
                        "selections": [
                          (v7/*: any*/),
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "status",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "isPublished",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "created",
                            "args": null,
                            "storageKey": null
                          },
                          {
                            "kind": "ScalarField",
                            "alias": null,
                            "name": "updated",
                            "args": null,
                            "storageKey": null
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "cursor",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "pageInfo",
                "storageKey": null,
                "args": null,
                "concreteType": "PageInfo",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "endCursor",
                    "args": null,
                    "storageKey": null
                  },
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "hasNextPage",
                    "args": null,
                    "storageKey": null
                  }
                ]
              },
              {
                "kind": "Condition",
                "passingValue": true,
                "condition": "withTotalCount",
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "totalCount",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          },
          {
            "kind": "LinkedHandle",
            "alias": "data",
            "name": "allNews",
            "args": (v8/*: any*/),
            "handle": "connection",
            "key": "NewsList_data",
            "filters": []
          }
        ]
      }
    ]
  },
  "params": {
    "operationKind": "query",
    "name": "NewsQuery",
    "id": null,
    "text": "query NewsQuery(\n  $filter: String\n  $first: Int\n  $offset: Int\n  $order: Order\n  $withTotalCount: Boolean!\n) {\n  viewer {\n    id\n    ...AdminNewsForm_viewer\n    ...AdminNewsList_viewer_2A7R4U\n  }\n}\n\nfragment AdminNewsForm_viewer on Viewer {\n  categories: allCategories(order: {by: \"name\"}) {\n    edges {\n      node {\n        __typename\n        ... on Category {\n          id\n          name\n        }\n        id\n      }\n    }\n  }\n}\n\nfragment AdminNewsList_viewer_2A7R4U on Viewer {\n  id\n  data: allNews(filter: $filter, first: $first, offset: $offset, order: $order) {\n    totalCount @include(if: $withTotalCount)\n    edges {\n      node {\n        ... on News {\n          id\n          name\n          status\n          isPublished\n          created\n          updated\n        }\n        id\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n",
    "metadata": {}
  }
};
})();
(node as any).hash = 'd25d05bfc19368e59883edbaebc35929';
export default node;
