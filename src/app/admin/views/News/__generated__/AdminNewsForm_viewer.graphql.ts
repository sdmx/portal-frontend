/* tslint:disable */
/* eslint-disable */

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type AdminNewsForm_viewer = {
    readonly categories: {
        readonly edges: ReadonlyArray<{
            readonly node: {
                readonly id?: string;
                readonly name?: string;
            };
        }>;
    } | null;
    readonly " $refType": "AdminNewsForm_viewer";
};
export type AdminNewsForm_viewer$data = AdminNewsForm_viewer;
export type AdminNewsForm_viewer$key = {
    readonly " $data"?: AdminNewsForm_viewer$data;
    readonly " $fragmentRefs": FragmentRefs<"AdminNewsForm_viewer">;
};



const node: ReaderFragment = {
  "kind": "Fragment",
  "name": "AdminNewsForm_viewer",
  "type": "Viewer",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "LinkedField",
      "alias": "categories",
      "name": "allCategories",
      "storageKey": "allCategories(order:{\"by\":\"name\"})",
      "args": [
        {
          "kind": "Literal",
          "name": "order",
          "value": {
            "by": "name"
          }
        }
      ],
      "concreteType": "Connection",
      "plural": false,
      "selections": [
        {
          "kind": "LinkedField",
          "alias": null,
          "name": "edges",
          "storageKey": null,
          "args": null,
          "concreteType": "Edge",
          "plural": true,
          "selections": [
            {
              "kind": "LinkedField",
              "alias": null,
              "name": "node",
              "storageKey": null,
              "args": null,
              "concreteType": null,
              "plural": false,
              "selections": [
                {
                  "kind": "InlineFragment",
                  "type": "Category",
                  "selections": [
                    {
                      "kind": "ScalarField",
                      "alias": null,
                      "name": "id",
                      "args": null,
                      "storageKey": null
                    },
                    {
                      "kind": "ScalarField",
                      "alias": null,
                      "name": "name",
                      "args": null,
                      "storageKey": null
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    }
  ]
};
(node as any).hash = '0b52b43da0333cb6a25720fc34bdb794';
export default node;
