import React from "react";
import { Col, PageHeader, message } from "antd";
import { injectIntl, IntlShape } from "react-intl";
import { graphql, fetchQuery } from "react-relay";
import { RouteComponentProps, withRouter } from "react-router-dom";

import { UploadableMap } from "types/relay-runtime/definitions";
import { NewsInput, Viewer } from "types/schema";

import environment from "relayEnvironment";
import { fileUrl } from "utils/string";

import Content from "components/Content";

import EditNews from "app/admin/mutations/news/EditNews";
import Form from "app/admin/views/News/AdminNewsForm";

interface Props extends RouteComponentProps<{ id: string }> {
  intl: IntlShape;
  viewer: Viewer;
  retry: (() => void) | null;
}

class NewsFormEdit extends React.Component<Props> {
  state = { initialValues: null };

  componentDidMount() {
    const { match } = this.props;
    const { id } = match.params;

    const query = graphql`
      query AdminNewsFormEditQuery($id: ID!) {
        viewer {
          news(id: $id) {
            name
            body
            links
            thumbnail
            categories {
              id
            }
          }
        }
      }
    `;

    fetchQuery(environment, query, { id }).then(({ viewer }: any) => {
      const initialValues: any = {};

      for (const key in viewer.news) {
        if (viewer.news[key]) {
          switch (key) {
            case "categories":
              initialValues[key] = viewer.news[key].map(({ id }: any) => id);
              break;

            case "thumbnail":
              initialValues[key] = fileUrl(viewer.news[key]);
              break;

            default:
              initialValues[key] = viewer.news[key];
              break;
          }
        }
      }

      this.setState({ initialValues });
    });
  }

  render() {
    const { history, intl, viewer, retry, match } = this.props;
    const { initialValues } = this.state;

    return (
      <div>
        <PageHeader
          title={intl.formatMessage({ id: "app.news.edit" })}
          onBack={() => history.push("/admin/news")}
        />

        <Content>
          <Col span={20}>
            {initialValues && (
              <Form
                viewer={viewer}
                initialValues={initialValues}
                handleSubmit={({ thumbnail, ...values }) => {
                  const input: NewsInput = { ...values };
                  const uploadables: UploadableMap = {};
                  const id = match.params.id;

                  if (thumbnail instanceof File) {
                    uploadables.thumbnail = thumbnail;
                  }

                  EditNews.commit(environment, input, uploadables, id)
                    .then(() => {
                      retry && retry();
                      history.push("/admin/news");
                      message.success(
                        intl.formatMessage({ id: "info.save.success" })
                      );
                    })
                    .catch(() => {
                      message.error(
                        intl.formatMessage({ id: "info.save.error" })
                      );
                    });
                }}
              />
            )}
          </Col>
        </Content>
      </div>
    );
  }
}

export default withRouter(injectIntl(NewsFormEdit));
