import React from "react";
import { Button, Form as AntForm, Input, Select, Row, Col } from "antd";
import { injectIntl, IntlShape } from "react-intl";
import { createFragmentContainer, graphql } from "react-relay";

import { Viewer, Connection, Maybe } from "types/schema";

import InputImage from "components/Input/InputImage";
import InputMultiple from "components/Input/InputMultiple";
import TextEditor from "components/Input/TextEditor";

export interface Props {
  nodeId?: string;
  intl: IntlShape;
  initialValues?: any;
  viewer: Viewer & { categories?: Maybe<Connection> };
  handleSubmit: (values: any) => any;
}

class AdminNewsForm extends React.Component<Props> {
  render() {
    const { initialValues, handleSubmit, intl, viewer } = this.props;
    const fmt = intl.formatMessage;

    return (
      <AntForm
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 18 }}
        initialValues={initialValues}
        onFinish={(values) => handleSubmit(values)}
      >
        <Row>
          <Col span={14}>
            {/* Name */}
            <AntForm.Item
              name="name"
              label={fmt({ id: "app.news.name" })}
              rules={[{ required: true }]}
            >
              <Input />
            </AntForm.Item>

            {/* Body */}
            <AntForm.Item
              name="body"
              label={fmt({ id: "app.news.body" })}
              rules={[{ required: true }]}
            >
              <TextEditor />
            </AntForm.Item>

            {/* Links */}
            <AntForm.Item name="links" label={fmt({ id: "app.news.links" })}>
              <InputMultiple />
            </AntForm.Item>
          </Col>

          <Col span={10}>
            {/* Categories */}
            <AntForm.Item
              name="categories"
              label={fmt({ id: "app.news.categories" })}
            >
              <Select
                showSearch
                mode="multiple"
                placeholder={fmt({ id: "app.news.categories" })}
                filterOption={(input, { children }: any) =>
                  children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                {viewer.categories?.edges?.map(({ node }: any) => (
                  <Select.Option key={node.id} value={node.id}>
                    {node.name}
                  </Select.Option>
                ))}
              </Select>
            </AntForm.Item>

            {/* Image Thumbnail */}
            <AntForm.Item
              name="thumbnail"
              label={fmt({ id: "app.news.thumbnail" })}
            >
              <InputImage />
            </AntForm.Item>

            {/* Submit Button */}
            <AntForm.Item wrapperCol={{ span: 18, offset: 6 }}>
              <Button type="primary" htmlType="submit">
                {fmt({ id: "action.submit" })}
              </Button>
            </AntForm.Item>
          </Col>
        </Row>
      </AntForm>
    );
  }
}

export default createFragmentContainer(injectIntl(AdminNewsForm), {
  viewer: graphql`
    fragment AdminNewsForm_viewer on Viewer {
      categories: allCategories(order: { by: "name" }) {
        edges {
          node {
            ... on Category {
              id
              name
            }
          }
        }
      }
    }
  `,
});
