import { IntlShape } from "react-intl";
import { Dispatch } from 'redux';
import { loadLocale } from 'locale';

export const REQUEST_LOCALE = "REQUEST_LOCALE";
export const RECEIVE_LOCALE = "RECEIVE_LOCALE";

export const requestLocale = (locale: string) => ({
  type: REQUEST_LOCALE,
  payload: { locale },
});

export const receiveLocale = (locale: string, intl: IntlShape) => ({
  type: RECEIVE_LOCALE,
  payload: { locale, intl },
});

export const setLocale = (dispatch: Dispatch) => (locale: string) => {
  dispatch(requestLocale(locale));

  loadLocale(locale, (intl) => {
    dispatch(receiveLocale(locale, intl));
  });
};
