import axios from "axios";
import { Environment, Network, RecordSource, Store } from "relay-runtime";
import { isAuthenticated, getAccessToken } from 'utils/auth';

const network = Network.create(
  (operation, variables, cacheConfig, uploadables) => {
    const endpoint: string = process.env.REACT_APP_GRAPHQL_ENDPOINT || "";
    // const endpoint: string = "http://localhost:9002/graphql";
    // const endpoint: string = "http://localhost:8090/graphql";
    // const endpoint: string = "http://localhost:8080/portal/graphql";

    const requestConfig: any = {
      timeout: process.env.NODE_ENV === "production" ? 0 : 3000,
      headers: {
        Accept: "application/json",
      },
    };

    // add access token if authenticated
    if (isAuthenticated()) {
      requestConfig.headers["Authorization"] = "Bearer " + getAccessToken();
    }

    let body;

    // Multipart Form Request
    if (!!uploadables && Object.keys(uploadables).length > 0) {
      const formData = new FormData();
      formData.append("query", operation.text || "");
      formData.append("variables", JSON.stringify(variables));

      Object.keys(uploadables).forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(uploadables, key)) {
          formData.append(key, uploadables[key]);
        }
      });

      body = formData;
    }
    // JSON Request
    else {
      requestConfig.headers["Content-Type"] = "application/json";
      body = {
        query: operation.text,
        variables,
      };
    }

    return axios.post(endpoint, body, requestConfig).then(({ data }) => data);
  }
);

const store = new Store(new RecordSource());

export default new Environment({ network, store });
