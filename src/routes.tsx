import React from "react";
import { FormattedMessage } from "react-intl";

const routes: LayoutType.MenuType[] = [
  {
    path: "/",
    label: <FormattedMessage id="app.home" />,
    exact: true,
    Component: React.lazy(() => import("views/Home")),
  },
  {
    path: "/login",
    label: <FormattedMessage id="app.login" />,
    Component: React.lazy(() => import("views/Login")),
  },
  {
    path: "/news",
    label: <FormattedMessage id="app.news" />,
    Component: React.lazy(() => import("views/News/index")),
  },
  {
    path: "/publication",
    label: <FormattedMessage id="app.publication" />,
    Component: React.lazy(() => import("views/Publication/index")),
  },
];

export default routes;
