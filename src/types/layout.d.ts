import { ReactType } from "react";

declare namespace LayoutType {
  export interface AdminLayoutType {
    sidebarLeftCollapse?: boolean;
  }

  export interface MenuType {
    path: string;
    label?: any;
    html?: boolean;
    exact?: boolean;
    onClick?: () => void;
    subs?: MenuType[];
    Icon?: ReactType;
    Component?: ReactType;
  }
}

export as namespace LayoutType;
export = LayoutType;
