export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: any }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** Input, Scalar, & Enum */
  DateTime: any;
  File: any;
};



export enum Direction {
  Asc = 'ASC',
  Desc = 'DESC'
}

export enum Privilege {
  Public = 'PUBLIC',
  Internal = 'INTERNAL'
}

export enum Status {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export enum ApprovalStatus {
  Draft = 'DRAFT',
  Approved = 'APPROVED',
  Rejected = 'REJECTED',
  Processed = 'PROCESSED'
}

export type Order = {
  by: Scalars['String'];
  direction?: Maybe<Direction>;
};

/** Queries */
export type Node = {
  id: Scalars['ID'];
};

export type RootQuery = {
  __typename?: 'RootQuery';
  viewer: Viewer;
  node?: Maybe<Node>;
};


export type RootQueryNodeArgs = {
  id: Scalars['ID'];
};

export type Viewer = Node & {
  __typename?: 'Viewer';
  id: Scalars['ID'];
  user?: Maybe<User>;
  allCategories?: Maybe<Connection>;
  category?: Maybe<Category>;
  allNews?: Maybe<Connection>;
  news?: Maybe<News>;
  allPublications?: Maybe<Connection>;
  publication?: Maybe<Publication>;
};


export type ViewerAllCategoriesArgs = {
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  filter?: Maybe<Scalars['String']>;
  order?: Maybe<Order>;
  after?: Maybe<Scalars['ID']>;
};


export type ViewerCategoryArgs = {
  id: Scalars['ID'];
};


export type ViewerAllNewsArgs = {
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  filter?: Maybe<Scalars['String']>;
  order?: Maybe<Order>;
  after?: Maybe<Scalars['ID']>;
  isPublished?: Maybe<Scalars['Boolean']>;
  status?: Maybe<Status>;
  ownedByUser?: Maybe<Scalars['Boolean']>;
  privilege?: Maybe<Privilege>;
};


export type ViewerNewsArgs = {
  id: Scalars['ID'];
};


export type ViewerAllPublicationsArgs = {
  first?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  filter?: Maybe<Scalars['String']>;
  order?: Maybe<Order>;
  after?: Maybe<Scalars['ID']>;
  isPublished?: Maybe<Scalars['Boolean']>;
  status?: Maybe<Status>;
  ownedByUser?: Maybe<Scalars['Boolean']>;
  privilege?: Maybe<Privilege>;
};


export type ViewerPublicationArgs = {
  id: Scalars['ID'];
};

/** Mutations */
export type Mutation = {
  __typename?: 'Mutation';
  createCategory?: Maybe<CategoryPayload>;
  updateCategory?: Maybe<CategoryPayload>;
  deleteCategory?: Maybe<CategoryPayload>;
  createNews?: Maybe<NewsPayload>;
  updateNews?: Maybe<NewsPayload>;
  deleteNews?: Maybe<NewsPayload>;
  createPublication?: Maybe<PublicationPayload>;
  updatePublication?: Maybe<PublicationPayload>;
  deletePublication?: Maybe<PublicationPayload>;
};


/** Mutations */
export type MutationCreateCategoryArgs = {
  input: CategoryInput;
};


/** Mutations */
export type MutationUpdateCategoryArgs = {
  input: CategoryInput;
  id: Scalars['ID'];
};


/** Mutations */
export type MutationDeleteCategoryArgs = {
  id: Scalars['ID'];
};


/** Mutations */
export type MutationCreateNewsArgs = {
  input: NewsInput;
};


/** Mutations */
export type MutationUpdateNewsArgs = {
  input: NewsInput;
  id: Scalars['ID'];
};


/** Mutations */
export type MutationDeleteNewsArgs = {
  id: Scalars['ID'];
};


/** Mutations */
export type MutationCreatePublicationArgs = {
  input: PublicationInput;
};


/** Mutations */
export type MutationUpdatePublicationArgs = {
  input: PublicationInput;
  id: Scalars['ID'];
};


/** Mutations */
export type MutationDeletePublicationArgs = {
  id: Scalars['ID'];
};

export type CategoryInput = {
  name: Scalars['String'];
  parentId?: Maybe<Scalars['ID']>;
};

export type CategoryPayload = {
  __typename?: 'CategoryPayload';
  node?: Maybe<Category>;
};

export type NewsInput = {
  name: Scalars['String'];
  body: Scalars['String'];
  links?: Maybe<Array<Maybe<Scalars['String']>>>;
  thumbnail?: Maybe<Scalars['File']>;
  categories?: Maybe<Array<Maybe<Scalars['ID']>>>;
};

export type NewsPayload = {
  __typename?: 'NewsPayload';
  node?: Maybe<News>;
};

export type PublicationInput = {
  name: Scalars['String'];
  body: Scalars['String'];
  links?: Maybe<Array<Maybe<Scalars['String']>>>;
  thumbnail?: Maybe<Scalars['File']>;
  categories?: Maybe<Array<Maybe<Scalars['ID']>>>;
};

export type PublicationPayload = {
  __typename?: 'PublicationPayload';
  node?: Maybe<Publication>;
};

/** Connection */
export type PageInfo = {
  __typename?: 'PageInfo';
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor: Scalars['ID'];
  endCursor: Scalars['ID'];
};

export type Edge = {
  __typename?: 'Edge';
  cursor: Scalars['String'];
  node: Node;
};

export type Connection = {
  __typename?: 'Connection';
  totalCount?: Maybe<Scalars['Int']>;
  pageInfo: PageInfo;
  edges: Array<Edge>;
};

/** Types */
export type User = Node & {
  __typename?: 'User';
  id: Scalars['ID'];
  username: Scalars['String'];
  name: Scalars['String'];
  email?: Maybe<Scalars['String']>;
  status: Status;
  organisation?: Maybe<Organisation>;
  position?: Maybe<Position>;
};

export type Organisation = Node & {
  __typename?: 'Organisation';
  id: Scalars['ID'];
  name: Scalars['String'];
  parent?: Maybe<Organisation>;
};

export type Position = Node & {
  __typename?: 'Position';
  id: Scalars['ID'];
  name: Scalars['String'];
  parent?: Maybe<Position>;
  hasOrganisation?: Maybe<Scalars['Boolean']>;
};

export type Category = Node & {
  __typename?: 'Category';
  id: Scalars['ID'];
  name: Scalars['String'];
  parent?: Maybe<Category>;
  children?: Maybe<Array<Maybe<Category>>>;
};

export type News = Node & {
  __typename?: 'News';
  id: Scalars['ID'];
  name: Scalars['String'];
  body?: Maybe<Scalars['String']>;
  links?: Maybe<Array<Maybe<Scalars['String']>>>;
  thumbnail?: Maybe<Scalars['File']>;
  status: Status;
  privilege: Privilege;
  isPublished: Scalars['Boolean'];
  created?: Maybe<Scalars['DateTime']>;
  updated?: Maybe<Scalars['DateTime']>;
  user: User;
  categories?: Maybe<Array<Maybe<Category>>>;
};

export type NewsApproval = Node & {
  __typename?: 'NewsApproval';
  id: Scalars['ID'];
  news: News;
  user: User;
  status: ApprovalStatus;
  created: Scalars['DateTime'];
  updated: Scalars['DateTime'];
};

export type Publication = Node & {
  __typename?: 'Publication';
  id: Scalars['ID'];
  name: Scalars['String'];
  body?: Maybe<Scalars['String']>;
  links?: Maybe<Array<Maybe<Scalars['String']>>>;
  thumbnail?: Maybe<Scalars['String']>;
  status: Status;
  privilege: Privilege;
  isPublished: Scalars['Boolean'];
  created: Scalars['DateTime'];
  updated: Scalars['DateTime'];
  user: User;
  categories?: Maybe<Array<Maybe<Category>>>;
  attachments?: Maybe<Array<Maybe<PublicationAttachment>>>;
};

export type PublicationAttachment = Node & {
  __typename?: 'PublicationAttachment';
  id: Scalars['ID'];
  displayName: Scalars['String'];
  filepath: Scalars['String'];
  created?: Maybe<Scalars['DateTime']>;
};

export type PublicationApproval = Node & {
  __typename?: 'PublicationApproval';
  id: Scalars['ID'];
  publication: Publication;
  user: User;
  status: ApprovalStatus;
  created: Scalars['DateTime'];
  updated: Scalars['DateTime'];
};


