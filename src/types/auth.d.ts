declare namespace AuthType {
  export interface LoginType {
    username: string;
    password: string;
    rememberMe?: boolean;
  }
}

export as namespace AuthType;
export = AuthType;
