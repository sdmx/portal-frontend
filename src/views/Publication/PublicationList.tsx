import React from "react";
import { IntlShape, injectIntl } from "react-intl";
import {
  graphql,
  createPaginationContainer,
  RelayPaginationProp,
} from "react-relay";

import { Viewer } from "types/schema";

import ListItem from "components/Content/ListItem";
import Newest from "components/Content/Newest";
import Highlight from "components/Content/Highlight";

interface Props {
  intl: IntlShape;
  relay: RelayPaginationProp;
  viewer: Viewer | undefined;
}

class PublicationList extends React.Component<Props> {
  urlResolver = ({ id }: any) => `publication/${id}`;

  render() {
    const { intl, relay, viewer } = this.props;
    const data = viewer?.allPublications?.edges.map(({ node }) => node);

    return (
      <div className="flex mb4">
        <div className="w-25">
          <ListItem
            data={data?.slice(0, 9)}
            title={intl.formatMessage({
              id: "app.publication",
              defaultMessage: "Publication",
            })}
            pageSize={10}
            onSearch={(filter, pageSize) => {
              relay.refetchConnection(pageSize || 10, null, {
                filter,
                withTotalCount: true,
              });
            }}
            urlResolver={this.urlResolver}
          />
        </div>

        <div className="w-50">
          <Newest data={data} urlResolver={this.urlResolver} />
        </div>

        <div className="w-25">
          <Highlight data={data?.slice(0, 9)} urlResolver={this.urlResolver} />
        </div>
      </div>
    );
  }
}

export default createPaginationContainer(
  injectIntl(PublicationList),
  {
    viewer: graphql`
      fragment PublicationList_viewer on Viewer
        @argumentDefinitions(
          filter: { type: "String" }
          first: { type: "Int" }
          offset: { type: "Int" }
          withTotalCount: { type: "Boolean!", defaultValue: false }
        ) {
        id
        allPublications(
          filter: $filter
          first: $first
          offset: $offset
          order: { by: "created", direction: DESC }
        ) @connection(key: "PublicationList_allPublications", filters: []) {
          totalCount @include(if: $withTotalCount)
          edges {
            node {
              ... on Publication {
                id
                name
                body
                links
                thumbnail
                updated
                categories {
                  id
                  name
                }
              }
            }
          }
        }
      }
    `,
  },
  {
    direction: "forward",
    getConnectionFromProps(props) {
      return props.viewer && props.viewer.allPublications;
    },
    getVariables(props, { count }, variables) {
      return {
        count,
        ...variables,
        withTotalCount: false,
      };
    },
    query: graphql`
      query PublicationListRefetchQuery(
        $filter: String
        $first: Int
        $offset: Int
        $withTotalCount: Boolean!
      ) {
        viewer {
          ...PublicationList_viewer
            @arguments(
              filter: $filter
              first: $first
              offset: $offset
              withTotalCount: $withTotalCount
            )
        }
      }
    `,
  }
);
