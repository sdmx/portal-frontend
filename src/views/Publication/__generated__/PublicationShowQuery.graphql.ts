/* tslint:disable */
/* eslint-disable */
/* @relayHash cf4c754e4640a4fd8903238c9b94c9f0 */

import { ConcreteRequest } from "relay-runtime";
export type PublicationShowQueryVariables = {
    id: string;
};
export type PublicationShowQueryResponse = {
    readonly viewer: {
        readonly publication: {
            readonly name: string;
            readonly body: string | null;
            readonly links: ReadonlyArray<string | null> | null;
            readonly thumbnail: string | null;
            readonly categories: ReadonlyArray<{
                readonly id: string;
            } | null> | null;
        } | null;
    };
};
export type PublicationShowQuery = {
    readonly response: PublicationShowQueryResponse;
    readonly variables: PublicationShowQueryVariables;
};



/*
query PublicationShowQuery(
  $id: ID!
) {
  viewer {
    publication(id: $id) {
      name
      body
      links
      thumbnail
      categories {
        id
      }
      id
    }
    id
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "id",
    "type": "ID!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "name",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "body",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "links",
  "args": null,
  "storageKey": null
},
v5 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "thumbnail",
  "args": null,
  "storageKey": null
},
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v7 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "categories",
  "storageKey": null,
  "args": null,
  "concreteType": "Category",
  "plural": true,
  "selections": [
    (v6/*: any*/)
  ]
};
return {
  "kind": "Request",
  "fragment": {
    "kind": "Fragment",
    "name": "PublicationShowQuery",
    "type": "RootQuery",
    "metadata": null,
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "viewer",
        "storageKey": null,
        "args": null,
        "concreteType": "Viewer",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "publication",
            "storageKey": null,
            "args": (v1/*: any*/),
            "concreteType": "Publication",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              (v7/*: any*/)
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "PublicationShowQuery",
    "argumentDefinitions": (v0/*: any*/),
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "viewer",
        "storageKey": null,
        "args": null,
        "concreteType": "Viewer",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "publication",
            "storageKey": null,
            "args": (v1/*: any*/),
            "concreteType": "Publication",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              (v7/*: any*/),
              (v6/*: any*/)
            ]
          },
          (v6/*: any*/)
        ]
      }
    ]
  },
  "params": {
    "operationKind": "query",
    "name": "PublicationShowQuery",
    "id": null,
    "text": "query PublicationShowQuery(\n  $id: ID!\n) {\n  viewer {\n    publication(id: $id) {\n      name\n      body\n      links\n      thumbnail\n      categories {\n        id\n      }\n      id\n    }\n    id\n  }\n}\n",
    "metadata": {}
  }
};
})();
(node as any).hash = 'ae6a35e3a6137635327ea194c6b4af15';
export default node;
