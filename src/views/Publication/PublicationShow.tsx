import React from "react";
import { FilePdfFilled } from "@ant-design/icons";
import moment from "moment";
import Img from "react-image-fallback";
import { graphql, fetchQuery } from "react-relay";
import { RouteComponentProps, withRouter } from "react-router-dom";

import environment from "relayEnvironment";
import { Publication } from "types/schema";

import SiderItem from "components/Content/SiderItem";

interface Props extends RouteComponentProps<{ id: string }> {}

interface State {
  data?: Publication | null;
}

class PublicationShow extends React.Component<Props> {
  state: State = { data: null };

  componentDidMount() {
    const { match } = this.props;
    const { id } = match.params;

    const query = graphql`
      query PublicationShowQuery($id: ID!) {
        viewer {
          publication(id: $id) {
            name
            body
            links
            thumbnail
            categories {
              id
            }
          }
        }
      }
    `;

    fetchQuery(environment, query, { id }).then(({ viewer }: any) => {
      this.setState({ data: viewer.publication });
    });
  }

  render() {
    const { data } = this.state;

    const categories: any = [];
    const attachments: any = [];

    return (
      <div>
        {data && (
          <div className="ph5 pt2">
            <div className="f7 mb2">
              {moment(data.created).format("DD MMM YYYY")}
            </div>
            <h1 className="f3 mb4 mv0 b primary">{data.name}</h1>

            <div className="cf bb pv1 mb3 b--moon-gray">
              <div className="fl"></div>
              {/* <div className="fr">
                <Icon type="mail" className="f3 ph1" />
                <Icon type="twitter" className="f3 ph1 light-blue" />
                <Icon type="facebook" className="f3 ph1 blue" />
                <Icon type="google-plus" className="f3 ph1 light-red" />
              </div> */}
            </div>

            <div className="cf">
              <div className="fl w-70" style={{ textAlign: "justify" }}>
                <div dangerouslySetInnerHTML={{ __html: data.body || "" }} />{" "}
                <br />
              </div>
              <div className="fl w-30 pl4">
                <div className="pl4">
                  <SiderItem>
                    {data.thumbnail && (
                      <Img
                        src={data.thumbnail}
                        alt={data.name}
                        fallbackImage="/img/default/content.png"
                        className="w-100"
                      />
                    )}
                  </SiderItem>

                  {attachments && attachments.length > 0 && (
                    <SiderItem label="Documents">
                      {attachments &&
                        attachments.map((attachment: any) => (
                          <a
                            href={attachment.filepath}
                            className="ba b--moon-gray pa2 br1 mt1 db"
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            <FilePdfFilled /> {attachment.displayName}
                          </a>
                        ))}
                    </SiderItem>
                  )}

                  {categories && categories.length > 0 && (
                    <SiderItem label="Categories">
                      {categories.map((category: any) => (
                        <div className="fl pv1 ph2 br2 ba b--moon-gray mr2">
                          {category.name}
                        </div>
                      ))}
                    </SiderItem>
                  )}
                </div>
              </div>
            </div>

            <div className="cf mt4 tr">
              {/* <Icon type="printer" className="f3" /> */}
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(PublicationShow);
