import React from "react";
import {
  QueryRenderer,
  graphql,
  RelayPaginationProp,
  Variables,
} from "react-relay";
import { Switch, Route } from "react-router-dom";

import relayEnvironment from "relayEnvironment";
import { Viewer } from "types/schema";

import Error from "components/Error";

const newsQuery = graphql`
  query PublicationListQuery(
    $filter: String
    $first: Int
    $offset: Int
    $withTotalCount: Boolean!
  ) {
    viewer {
      id
      ...PublicationList_viewer
        @arguments(
          filter: $filter
          first: $first
          offset: $offset
          withTotalCount: $withTotalCount
        )
    }
  }
`;

const PublicationList = React.lazy(() => import("./PublicationList"));
const PublicationShow = React.lazy(() => import("./PublicationShow"));

interface Props {
  viewer: Viewer;
  relay: RelayPaginationProp;
}

class Publication extends React.Component<Props> {
  render() {
    return (
      <QueryRenderer<{ variables: Variables; response: { viewer: Viewer } }>
        environment={relayEnvironment}
        variables={{ withTotalCount: true }}
        query={newsQuery}
        render={({ error, props }) => {
          if (error) {
            return <Error message={error.message} />;
          } else {
            return (
              <Switch>
                <Route exact path="/publication">
                  <PublicationList viewer={props?.viewer} />
                </Route>
                <Route path="/publication/:id">
                  <PublicationShow />
                </Route>
              </Switch>
            );
          }
        }}
      />
    );
  }
}

export default Publication;
