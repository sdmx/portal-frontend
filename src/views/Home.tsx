import React from "react";
import { Input, Layout, Row, Col } from "antd";
import { IntlShape, injectIntl } from "react-intl";
import { withRouter, RouteComponentProps } from "react-router-dom";

import Banner from "components/Home/Banner";
import Commodity from "components/Home/Commodity";
import CurrencyIndicator from "components/Home/CurrencyIndicator";
import DailyIndicator from "components/Home/DailyIndicator";
import HomepageMenu from "components/Home/HomepageMenu";
import FeaturedPublication from "components/Home/FeaturedPublication";
import FeaturedNews from "components/Home/FeaturedNews";
import NewsPreview from "components/Home/NewsPreview";
import DatasetSubjects from "components/Home/DatasetSubjects";
import StatLinks from "components/Home/StatLinks";

import style from "./Home.module.css";

interface Props extends RouteComponentProps {
  intl: IntlShape;
}

class Home extends React.Component<Props> {
  render() {
    const { history, intl } = this.props;

    return (
      <Layout.Content>
        <div className={style.inputSearch}>
          <Input.Search
            allowClear
            size="large"
            placeholder={intl.formatMessage({ id: "action.search" })}
            onSearch={(value) => history.push(`/search?q=${value}`)}
          />
        </div>

        <CurrencyIndicator />
        <Banner />

        <Row>
          {/* Left Content */}
          <Col span={5}>
            <Commodity />
            <DailyIndicator />
            <StatLinks />
          </Col>

          {/* Main Content */}
          <Col span={14}>
            <HomepageMenu />
            <FeaturedNews />
            <FeaturedPublication />
          </Col>

          {/* Right Content */}
          <Col span={5}>
            <NewsPreview />
            <DatasetSubjects />
          </Col>
        </Row>
      </Layout.Content>
    );
  }
}

export default withRouter(injectIntl(Home));
