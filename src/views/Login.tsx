import React from "react";
import { Input, Button, Form, Checkbox, Card, Layout, Row, Col, message } from "antd";
import { LockFilled } from "@ant-design/icons";
import { FormattedMessage, injectIntl, IntlShape } from "react-intl";
import { withRouter, RouteComponentProps } from "react-router-dom";

import { login, isAuthenticated, redirectLogin } from "utils/auth";

import style from "./Login.module.css";

interface Props extends RouteComponentProps {
  intl: IntlShape;
}

const RememberMe = ({ onChange }: any) => (
  <Checkbox onChange={(e) => onChange(e.target.checked)}>
    <FormattedMessage
      id="app.login.form.remember_me"
      defaultMessage="Remember Me"
    />
  </Checkbox>
);

class Login extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    const { history } = props;

    if (isAuthenticated()) {
      history.push(redirectLogin);
    }
  }

  login(data: AuthType.LoginType) {
    login(data)
      .catch(() => {
        message.error("User not found");
      })
      .then((data) => {
        if (data) {
          window.location.href = redirectLogin;
        }
      });
  }

  render() {
    const { intl } = this.props;

    return (
      <Layout className={style.layout}>
        <Card
          className={style.login}
          title={
            <div className={style.title}>
              <LockFilled />
              <span className="ml2">
                <FormattedMessage id="action.login" defaultMessage="Login" />
              </span>
            </div>
          }
        >
          <Form onFinish={(values: any) => this.login(values)}>
            <Form.Item name="username">
              <Input
                size="large"
                placeholder={intl.formatMessage({
                  id: "app.login.form.username",
                  defaultMessage: "Username",
                })}
              />
            </Form.Item>
            <Form.Item name="password">
              <Input.Password
                size="large"
                placeholder={intl.formatMessage({
                  id: "app.login.form.password",
                  defaultMessage: "Password",
                })}
              />
            </Form.Item>

            <Row className={style.action}>
              <Col span={12}>
                <Form.Item name="rememberMe">
                  <RememberMe />
                </Form.Item>
              </Col>
              <Col span={12} style={{ textAlign: "right" }}>
                <Form.Item>
                  <Button size="large" type="primary" htmlType="submit">
                    <FormattedMessage
                      id="action.login"
                      defaultMessage="Login"
                    />
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
      </Layout>
    );
  }
}

export default injectIntl(withRouter(Login));
