import React from "react";
import {
  QueryRenderer,
  graphql,
  RelayPaginationProp,
  Variables,
} from "react-relay";
import { Switch, Route } from "react-router-dom";

import relayEnvironment from "relayEnvironment";
import { Viewer } from "types/schema";

import Error from "components/Error";

const newsQuery = graphql`
  query NewsListQuery(
    $filter: String
    $first: Int
    $offset: Int
    $withTotalCount: Boolean!
  ) {
    viewer {
      id
      ...NewsList_viewer
        @arguments(
          filter: $filter
          first: $first
          offset: $offset
          withTotalCount: $withTotalCount
        )
    }
  }
`;

const NewsList = React.lazy(() => import("./NewsList"));
const NewsShow = React.lazy(() => import("./NewsShow"));

interface Props {
  viewer: Viewer;
  relay: RelayPaginationProp;
}

class News extends React.Component<Props> {
  render() {
    return (
      <QueryRenderer<{ variables: Variables; response: { viewer: Viewer } }>
        environment={relayEnvironment}
        variables={{ withTotalCount: true }}
        query={newsQuery}
        render={({ error, props }) => {
          if (error) {
            return <Error message={error.message} />;
          } else {
            return (
              <Switch>
                <Route exact path="/news">
                  <NewsList viewer={props?.viewer} />
                </Route>
                <Route path="/news/:id">
                  <NewsShow />
                </Route>
              </Switch>
            );
          }
        }}
      />
    );
  }
}

export default News;
