import { createStore } from "redux";

import { createMiddleware } from "./middlewares";
import reducers from "./reducers";

// get default state from localstorage
const state = window.localStorage.getItem("app");
const defaultState = state !== null ? JSON.parse(state): {};

export default createStore(reducers, defaultState, createMiddleware());
