const messages: Record<string, string> = {
  "action.new": "Baru",
  "action.delete": "Hapus",
  "action.create": "Baru",
  "action.edit": "Ubah",
  "action.search": "Cari",
  "action.submit": "Simpan",

  "info.empty": "Kosong",
  "info.confirmation": "Apakah anda yakin ?",
  "info.save.success": "Data berhasil disimpan",
  "info.save.error": "Gagal menyimpan data",
  "info.create.success": "Data berhasil dibuat",
  "info.create.error": "Gagal membuat data",
  "info.delete.success": "Data berhasil dihapus",
  "info.delete.error": "Gagal menghapus data",

  "error.404": "Halaman tidak ditemukan.",

  "app.home": "Beranda",
  "app.action": "Aksi",
  "app.created": "Dibuat",
  "app.updated": "Diubah",
  "app.status": "Status",
  "app.privilege": "Privilege",

  commodity: "Komoditas",
  statistics: "Statistik",
  "open data": "Open Data",
  "daily indicator": "Indikator Harian",
  "statistic information": "Indikator Statistik",

  "app.category": "Kategori",
  "app.category.create": "Buat Kategori",
  "app.category.edit": "Ubah Kategori",
  "app.category.name": "Nama",
  "app.category.parent": "Parent",

  "app.news": "Berita",
  "app.news.create": "Buat Berita",
  "app.news.edit": "Ubah Berita",
  "app.news.name": "Nama",
  "app.news.links": "Link",
  "app.news.body": "Konten",
  "app.news.thumbnail": "Thumbnail",
  "app.news.categories": "Kategori",
  "app.news.isPublished": "Terbit",

  "app.user": "User",
  "app.publication": "Publikasi",
  "app.dataService": "Data",
};

export default messages;
