const messages: Record<string, string> = {
  "action.new": "New",
  "action.delete": "Delete",
  "action.create": "Create",
  "action.edit": "Edit",
  "action.search": "Search",
  "action.submit": "Submit",

  "info.empty": "Empty",
  "info.confirmation": "Are you sure ?",
  "info.save.success": "Data saved successfully",
  "info.save.error": "Failed to save the data",
  "info.create.success": "Data created successfully",
  "info.create.error": "Failed to create the data",
  "info.delete.success": "Data deleted successfully",
  "info.delete.error": "Failed to delete the data",

  "error.404": "Page not found.",

  "app.home": "Home",
  "app.action": "Action",
  "app.created": "Created At",
  "app.updated": "Updated At",
  "app.status": "Status",
  "app.privilege": "Privilege",

  "commodity": "Commodity",
  "statistics": "Statistics",
  "open data": "Open Data",
  "daily indicator": "Daily Indicator",
  "statistic information": "Statistic Indicator",

  "app.category": "Category",
  "app.category.create": "Create Category",
  "app.category.edit": "Edit Category",
  "app.category.name": "Name",
  "app.category.parent": "Parent",

  "app.news": "News",
  "app.news.create": "Create News",
  "app.news.edit": "Edit News",
  "app.news.name": "Name",
  "app.news.links": "Links",
  "app.news.body": "Content",
  "app.news.thumbnail": "Thumbnail",
  "app.news.categories": "Category",
  "app.news.isPublished": "Published",

  "app.user": "User",
  "app.publication": "Publication",
  "app.dataService": "Data",
};

export default messages;
