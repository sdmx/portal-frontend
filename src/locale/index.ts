import { createIntl, createIntlCache, IntlShape } from "react-intl";

type localeCallback = (intl: IntlShape) => void;

export const defaultLocale = "en";
const cache = createIntlCache();

export const loadLocale = (locale: string, callback: localeCallback) => {
  return import(`./${locale}`).then((messages) => {
    const intl = createIntl({ locale, messages: messages.default }, cache);
    callback(intl);
  });
};
