import axios from "axios";
import qs from "querystring";
import { setInterval } from "timers";

interface TokenStorage {
  accessToken: string;
  tokenExpiry: number;
}

let _storage: TokenStorage | null = null;
let _silentRefresh: any | null = null;

export const redirectLogin = "/";
export const redirectLogout = "/login";

const syncLogout = (event: any) => {
  if (event.key === "logout") {
    window.location.href = redirectLogout;
  }
};

window.addEventListener("storage", syncLogout);

export const login = (data: AuthType.LoginType) => {
  return axios
    .post(
      `${process.env.REACT_APP_AUTH_ENDPOINT}/login`,
      qs.stringify({ ...data }),
      {
        withCredentials: true,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    )
    .then(({ data }) => {
      handleResponse(data);
      return data;
    });
};

export const logout = () => {
  return axios
    .post(`${process.env.REACT_APP_AUTH_ENDPOINT}/logout`, null, {
      withCredentials: true,
    })
    .then(() => {
      clearInterval(_silentRefresh);
      _storage = null;

      // trigger logout on another tabs
      window.localStorage.setItem("logout", Date.now().toString());
    });
};

export const refresh = (onSuccess?: (data: any) => void, onFailure?: () => void) => {
  return axios
    .post(`${process.env.REACT_APP_AUTH_ENDPOINT}/refresh`, null, {
      withCredentials: true,
      timeout: process.env.NODE_ENV === "production" ? 0 : 3000,
    })
    .then(({ data }) => {
      handleResponse(data);

      if (onSuccess) {
        onSuccess(data);
      }

      return data;
    })
    .catch(() => {
      if (onFailure) {
        onFailure();
      }
    });
};

export const setSilentRefreshInterval = (time: number) => {
  _silentRefresh = setInterval(() => {
    refresh();
  }, (time - 60) * 1000);
};

/** Helpers */

export const getAccessToken = () => {
  return _storage ? _storage.accessToken : null;
};

export const getExpiry = () => {
  return _storage ? _storage.tokenExpiry : null;
};

export const handleResponse = (data: TokenStorage) => {
  _storage = data;
  setSilentRefreshInterval(data.tokenExpiry);
};

export const isAuthenticated = () => {
  return _storage !== null;
};
