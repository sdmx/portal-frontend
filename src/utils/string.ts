const __tmp = document.createElement("div");

export const striptags = (html: string) => {
  __tmp.innerHTML = html;
  return __tmp.innerText;
};

export const fileUrl = (filepath: string, download = false) => {
  return process.env.REACT_APP_FILE_ENDPOINT + "/" + filepath + (download ? "?download" : "");
};

export const truncate = (value: string, len: number, suffix = "...") => {
  return value.length > len ? value.substring(0, len) + suffix : value;
};
